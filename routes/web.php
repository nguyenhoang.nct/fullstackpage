<?php



/*

|--------------------------------------------------------------------------

| Web Routes

|--------------------------------------------------------------------------

|

| Here is where you can register web routes for your application. These

| routes are loaded by the RouteServiceProvider within a group which

| contains the "web" middleware group. Now create something great!

|

*/



Route::get('/','PageController@getIndex')->name('home');

Route::get('course/{id}-{alias}', 'PageController@getDetailCourse')->name('course');

Route::post('contact', 'PageController@postContact');

Route::post('register', 'PageController@postRegister');
Route::get('register', 'PageController@register')->name('register');
Route::get('register-free', 'PageController@registerFree')->name('register_free');

Route::get('/blogs', 'BlogController@index')->name('blog.index');
Route::get('/chuyen-muc/{alias}', 'BlogController@category')->name('blog.category');
Route::get('/the-loai/{alias}', 'BlogController@type')->name('blog.type');
Route::get('/blog-{id}-{alias}.html', 'BlogController@detail')->name('blog.detail');
Route::get('/tag/{alias}', 'BlogController@tag')->name('blog.tag');
Route::get('/search/{keyword}/{page?}', 'BlogController@search')->name('blog.search');




