$.ajaxSetup({

    headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

    }

});



$(document).ready(function () {

    $('#contact-form').validate({

       rules: {

            name: {

                required: true

            },

            phone: {

                required: true

            }

       },

        messages: {

            name: {

                required: 'Họ tên không được để trống',

            },

            phone: {

                required: 'SĐT không được để trống'

            }

        }

    });

    $('.noti_contact').hide();

    $('.loading').hide();

    $('#contact-form').submit(function (e) {

        e.preventDefault();

        if($('#contact-form').valid()){

            $.ajax({

                url: 'contact',

                dataType: 'json',

                type: 'post',

                data: {

                    'name': $('#contact-name').val(),

                    'phone': $('#contact-phone').val(),

                    'status' : 0

                },

                success: function (result) {

                    console.log(result);

                    if (result.success == true)

                    {

                        $('.contact-form').hide();

                        $('.loading').show();

                        setTimeout(function () {

                            $('.noti_contact').show();

                            $('.loading').hide();

                        }, 2000);



                        // Swal.fire({

                        //         title: 'Success',

                        //         text: 'Chúng tôi sẽ liên lạc lại với bạn !',

                        //         customClass: 'swal',

                        //         type: 'success'

                        //     }

                        // )

                    }

                    else

                    {



                    }

                }

            });

        }

    });

    //register course

    $('#register-form').validate({

        rules: {

            register_name: {

                required: true,

            },

            register_email: {

                required: true,

                email: true

            },

            register_phone:{

                required: true,

                phoneNumber: {matches:"[0-9]+",minlength:10, maxlength:11}

            },

            register_course: {

                required: true,

            },

            register_birthday: {

                required: true,

                date: true

            },

            register_address: {

                required: true,

            },

        },

        messages: {

            register_name: {

                required: 'Họ tên không được để trống'

            },

            register_email: {

                required: 'Email không được để trống',

                email: 'Định dạng email không đúng'

            },

            register_phone: {

                required: 'SĐT không được để trống',

                number: 'Chỉ được nhập số'

            },

            register_course: {

                required: 'Khóa học không được để trống'

            },

            register_birthday: {

                required: 'Ngày sinh không được để trống',

                data: 'Ngày sinh không hợp lệ'

            },

            register_address: {

                required: 'Địa chỉ không được để trống',

            },

        }

    });

    $('#register-form').submit(function (e) {

        e.preventDefault();



        if($('#register-form').valid()){

            $.ajax({

                url: 'register',

                type: 'post',

                dataType: 'json',

                data: {

                    'register_name': $('#register-name').val(),

                    'register_email': $('#register-email').val(),

                    'register_phone': $('#register-phone').val(),

                    'register_course': $('#register-course').val(),

                    'register_birthday': $('#register-birthday').val(),

                    'register_address': $('#register-address').val(),

                    'register_job': $('#register-job').val(),

                    'register_note': $('#register-note').val(),

                    'link_fb': $('#link-fb').val(),

                },

                success: function (result) {

                    console.log(result);

                    if(result.success == true)

                    {

                        //$('#register').modal('hide');

                        Swal.fire({

                                title: 'Success',

                                text: 'Đăng ký thành công !',

                                customClass: 'swal',

                                type: 'success',



                            }).then(function(){

                                 location.reload();

                            })

                    }

                    else

                    {



                    }

                }

            });

        }

    });

    //modal detail course

    $('.loading-detail').hide();

    $('.detail-course').click(function () {

        let id = $(this).data('id');

        var show = $('.loading-detail').show();

        $('.loading-detail').html(show);

        $.ajax({

           url: 'detail-course/' + id,

           type: 'get',

            dataType: 'json',

            success: function (result) {
                console.log(result);
                $('.detail-title').html(result.course_name.title);
                var text = "";
                console.log(result.subject_detail.name.length);
                if(result.subject_detail.name.length > 0) {
                    for ( let i = 0; i < result.subject_detail.name.length; ++i) {
                        if(result.subject_detail.promotion_price[i] > 0) {
                            text += '<h4>✔ '+result.subject_detail.name[i]+' ('+result.subject_detail.number_session[i]+' buổi) - <del>' +new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail.price[i]) + ' </del> '+new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail.promotion_price[i])+"</h4><ul class='list__course'>";
                        }
                        else {
                            text += '<h4>✔ '+result.subject_detail.name[i]+' ('+result.subject_detail.number_session[i]+' buổi) - '+new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail.price[i])+"</h4><ul class='list__course'>";
                         
                        }
                        for(let j = 0; j < result.subject_detail.description[i].split(';').length; ++j) {
                            text+= "<li> ✎ "+result.subject_detail.description[i].split(';')[j]+"</li>";
                        }   
                        text+= "</ul><div style='width: 100%; height: 1px; background: red;; margin-top: 15px'></div>";


                    }
                    $('#append').html(text);
                }
                else  $('#append').html('Hiện chưa có môn học nào cho khoá này');
                


                

                // var text = "";
                // console.log(result);
                // $('.detail-title').html(result.course_name.title);
                // console.log(result.subject_detail);
                // if(result.subject_detail.length > 0) {

                    
                //     for ( let i = 0; i < result.subject_detail.length; ++i) {
                //         if(result.subject_detail[i].promotion_price > 0) {
                //             text += '<h4>✔ '+result.subject_detail[i].name+' ('+result.subject_detail[i].number_session+' buổi) - <del>' +new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail[i].price) + ' </del> '+new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail[i].promotion_price)+"</h4><ul class='list__course'>";
                //         }
                //         else {
                //             text += '<h4>✔ '+result.subject_detail[i].name+' ('+result.subject_detail[i].number_session+' buổi) - '+new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(result.subject_detail[i].price)+"</h4><ul class='list__course'>";
                //         }
                //         for(let j = 0; j < result.subject_detail[i].description.split(';').length; ++j) {
                //             text+= "<li> ✎ "+result.subject_detail[i].description.split(';')[j]+"</li>";
                //         }   
                //         text+= "</ul><div style='width: 100%; height: 1px; background: red;; margin-top: 15px'></div>";                    
                //     };
                //     $('#append').html(text);
                // }
                // else  $('#append').html('Hiện chưa có môn học nào cho khoá này');
                
               
            }

        });

        // $('#detail').click(function(event){

        //     event.stopPropagation();

        //     window.location.reload();

        // });



        $('.close-detail').click(function () {

            $('.course-detail').html('');

        });

    });

});
