<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RollUp extends Model
{
    //
    CONST ROLLUP_STATUS = [
    	0 => 'Chưa điểm danh',
    	1 => 'Đã điểm danh'
    ];
    CONST STATUS = [
    	0 => 'Đi học',
    	1 => 'Nghỉ học có phép',
    	2 => 'Nghỉ học không phép'
    ];
    protected $table = "roll_ups";
    public function user() {
    	return $this->belongsTo('App\User', 'user_id','id');
    }

}
