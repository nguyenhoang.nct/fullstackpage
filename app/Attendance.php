<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    //
    protected $table = 'attendances';
    public function Classes() {
    	return $this->hasMany('App\Classes', 'class_id', 'id');
    }
    public function User() {
    	return $this->hasMany('App\User', 'user_id', 'id');
    }
}
