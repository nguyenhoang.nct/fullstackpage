<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    public function course_detail()
    {
        return $this->hasOne('App\CourseDetail', 'course_id', 'id');
    }
    public function classes() {
        return $this->hasMany('App\Classes','course_id', 'id');
    }

    public function codes()
    {
        return $this->hasMany('App\Code', 'course_id', 'id');
    }

    public function register_course()
    {
        return $this->belongsTo('App\RegisterCourse', 'course_id', 'id');
    }
    public function subject()
    {
        return $this->belongsToMany('App\Subject');
    }

    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
}
