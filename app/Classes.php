<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    //
    protected $table = 'classes';
    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
    const DAY = [
        1 => 'Chủ nhật',
        2 => 'Thứ 2',
        3 => 'Thứ 3',
        4 => 'Thứ 4',
        5 => 'Thứ 5',
        6 => 'Thứ 6',
        7 => 'Thứ 7'

    ];
    public function course() {
    	return $this->belongsTo('App\Course','course_id', 'id');
    }

    public function classRoom() {
        return $this->belongsTo('App\ClassRoom', 'classroom_id', 'id');
    }

    public function learning_shift()
    {
        return $this->belongsTo('App\LearningShift', 'learning_shift_id', 'id');
    }
    public function attendance() {
        return $this->belongsTo('App\Attendance', 'class_id', 'id');
    }


}
