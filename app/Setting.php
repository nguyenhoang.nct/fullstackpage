<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';

    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
}
