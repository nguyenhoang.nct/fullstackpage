<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Logo extends Model
{
    protected $table = 'logos';

    CONST STATUS = [
      1 => 'ON',
      0 => 'OFF'
    ];
}
