<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseDetail extends Model
{
    protected $table = 'course_details';

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }

    public function setImagesAttribute($images)
    {
        if (is_array($images)) {
            $this->attributes['images'] = json_encode($images);
        }
    }

    public function getImagesAttribute($image)
    {
        return json_decode($image, true);
    }
}
