<?php

namespace App\Admin\Controllers;
use Illuminate\Support\Facades\DB;
use App\Course;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CourseAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Courses';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Course);

        $grid->column('id', __('ID'))->display(function ($id){
            return "<a href='/admin/coursedetails/$id'>$id</a>";
        });

        $grid->column('title', __('Title'));
        $grid->column('feature_image', __('Feature Image'))->image();
        $grid->column('price')->display(function ($price){
            return number_format($price);
        });
        $grid->column('promotion_price')->display(function ($price){
            return number_format($price);
        });

        // $grid->column('description', __('Description'))->display(function ($des){
        //     return htmlspecialchars_decode($des);
        // });
        // $grid->column('detail', 'Detail');
        $grid->column('is_free')->editable('select', Course::STATUS);
        // $grid->column('number_sessions', 'Số buổi học');
        $grid->column('status', __('Status'))->editable('select', Course::STATUS);

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->expand();
            $filter->disableIdFilter();

            $filter->like('title', 'Title');
            $filter->equal('status', 'Status')->radio(Course::STATUS);
            $filter->between('price');
            $filter->between('promotion_price');
            $filter->between('number_sessions');
            $filter->scope('promotion_price', 'Course Promotion')->where('promotion_price', '!=', 0);
            $filter->equal('created_at', 'Created_at')->datetime();
        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data courses</div>";
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Course::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', __('Title'));
        $show->field('feature_image', __('Feature image'))->image();
        $show->column('price')->as(function ($price){
            return number_format($price);
        });
        $show->column('promotion_price')->as(function ($price){
            return number_format($price);
        });
        $show->field('description', __('Description'));
        $show->field('participants', __('Participants'));
        $show->field('detail', 'Detail');
        $show->field('results', __('Results'));
        $show->field('is_free');
        $show->field('number_sessions');
        $show->number('total_day', 'Total Day');
        $show->number('day_per_week', 'Day per week');
        $show->number('min_student', 'Min Student');
        $show->number('max_student', 'Max Student');
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Course);

        $form->text('title', __('Title'))->rules('required');
        $form->image('feature_image', __('Feature image'))->move('courses/feature')
        ->rules('required')->removable();
        $form->text('price');
        $form->text('promotion_price');
        $form->textarea('description', __('Description'));
        $form->textarea('participants', __('Participants'));
        $form->textarea('detail', 'Detail');
        $form->textarea('results', __('Results'));
        $form->switch('is_free', __('Is Free'))->default(0);
        $form->number('number_sessions', 'Number sessions');
        $form->number('total_day', 'Total Day');
        $form->number('day_per_week', 'Day per week');
        $form->number('min_student', 'Min Student');
        $form->number('max_student', 'Max Student');
        $form->switch('status', __('Status'))->default(1);


        return $form;
    }
}
