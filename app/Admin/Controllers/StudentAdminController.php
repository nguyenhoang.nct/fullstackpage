<?php

namespace App\Admin\Controllers;

use App\Student;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class StudentAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Students';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Student);

        $grid->column('id', __('ID'));
        $grid->column('full_name', __('Full name'));
        $grid->column('avatar', __('Avatar'))->image();
        $grid->column('job', __('Job'));
        $grid->column('note', __('Note'))->display(function ($des){
            return htmlspecialchars_decode($des);
        });
        $grid->column('status', __('Status'))->editable('select', Student::STATUS);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
           $filter->expand();
           $filter->disableIdFilter();

           $filter->like('full_name', 'Full name');
           $filter->equal('status', 'Status')->radio(Student::STATUS);
           $filter->equal('create_at', 'Created At')->datetime();
        });
        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data students</div>";
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Student::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('full_name', __('Full name'));
        $show->field('avatar', __('Avatar'))->image();
        $show->field('job', __('Job'));
        $show->field('note', __('Note'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Student);

        $form->text('full_name', __('Full name'));
        $form->image('avatar', __('Avatar'))->move('students')->removable()
        ->rules('required');
        $form->text('job', __('Job'));
        $form->textarea('note', __('Note'));
        $form->switch('status', __('Status'))->default(1);

        return $form;
    }
}
