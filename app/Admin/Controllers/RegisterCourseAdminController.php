<?php

namespace App\Admin\Controllers;

use App\Course;
use App\RegisterCourse;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class RegisterCourseAdminController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Register Course';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new RegisterCourse);
        $grid->header(function (){
            if(session('thongbao'))
                return "<div class='alert alert-success'>".session('thongbao')."</div>";
        });
        $grid->column('id', __('ID'));
        $grid->model()->orderBy('id', 'desc');
        $grid->column('full_name', __('Full Name'));
        $grid->column('phone', __('Phone'));
        $grid->column('email', __('Email'));
        $grid->column('course_id', __('Course Name'))->display(function ($course_id){
           $course = Course::where('id', $course_id)->first();
           return isset($course) ? $course->title : 'N/A';
        });
        $grid->column('birthday',__('Birthday'));
        $grid->column('address',__('Address'));
        $grid->column('job',__('Job'));
        $grid->column('linkfb', 'Link facebook');
        
        $grid->column('status',__('Status'))->editable('select', RegisterCourse::STATUS);;
        $grid->column('note',__('Note'));
        $grid->column('Convert To Leads')->display(function(){
                return "<a href='convert-to-leads/$this->id/register_courses'>Convert To Leads</a>";
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->expand();
            $filter->disableIdFilter();
            $filter->column(1/2, function ($filter){
                $filter->like('full_name', 'Full Name');
                $filter->equal('email', 'Email');
                $filter->equal('phone', 'Phone');
                $filter->equal('course_id', 'Course Name')
                ->select(Course::where('status', self::ENABLE)->pluck('title', 'id'));
                $filter->like('linkfb', 'Link facebook');
            });
            $filter->column(1/2, function ($filter){
                $filter->date('birthday','Birthday');
                $filter->like('address','Address');
                $filter->like('job','Job');
                $filter->equal('status')->radio(RegisterCourse::STATUS);
                $filter->between('created_at', 'Created_at')->datetime();
            });
        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data yêu cầu đăng ký</div>";
        });
//        $grid->actions(function ($action){
//            $action->disableDelete();
//            $action->disableEdit();
//        });
       $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(RegisterCourse::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('full_name', __('Full name'));
        $show->field('phone', __('Phone'));
        $show->field('email', __('Email'));
        $show->field('course_id', __('Course Name'));
        $show->field('birthday', __('Birthday'));
        $show->field('address', __('Address'));
        $show->field('job', __('Job'));
        $show->field('linkfb', 'Link FB');
        $show->field('note', __('Note'));
        $show->status('Status')->as(function ($status){
            if($status == 0)
                return 'Chưa học thử';
            else
                return 'Đã học thử';
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new RegisterCourse);

        $form->text('full_name', __('Full name'));
        $form->text('phone', __('Phone'));
        $form->email('email', __('Email'));
        $form->select('course_id', __('Course name'))
        ->options(Course::where('status', self::ENABLE)->pluck('title', 'id'));
        $form->date('birthday',__('Birthday'));
        $form->textarea('address',__('Address'));
        $form->text('job',__('Job'));
        $form->text('linkfb','Link FB');
        $form->textarea('note',__('Note'));
        $form->select('status')->options(RegisterCourse::STATUS);

        return $form;
    }
}
