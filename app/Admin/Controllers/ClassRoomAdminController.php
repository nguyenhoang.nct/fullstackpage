<?php

namespace App\Admin\Controllers;

use App\Classroom;
use App\Course;
use App\LearningFacility;
use App\LearningShift;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;
use Illuminate\Support\Facades\DB;

class ClassRoomAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Classrooms';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Classroom);

        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('title', __('Title'))->modal(function ($model) {
            $class = $model->classes->map(function ($class){
                return $class->only(["title", "course->title", "learning_shift",
                    "start_learning_date",
                    "end_learning_date","day","created_at"]);
            });
            return new Table(['Title', 'Course', 'Learning_shift', 'Start', 'End', 'Day', 'Created_at'], $class->toArray());
        });
        $grid->column('description', __('Description'));
        $grid->column('learning_facility_id', __('Learning Facility'))->display(function ($learning_facility){
            return isset($learning_facility) ? (LearningFacility::select('name')->where('id', $learning_facility)
                ->first())->name : "N/A";
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
           $filter->expand();
           $filter->disableIdFilter();
           $filter->like('title', 'Title');
           $filter->equal('learning_facility_id', 'Learning Facility')
               ->select(LearningFacility::where('status', 1)->pluck('name', 'id'));
        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data classrooms</div>";
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Classroom::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', __('Title'));
        $show->field('description', __('Description'));
        $show->field('learning_facility_id', __('Learning Facility'))->as(function ($learning_facility){
            return isset($learning_facility) ? (LearningFacility::select('name')->where('id', $learning_facility)
                ->first())->name : "N/A";
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Classroom);

        $form->text('title', __('Title'));
        $form->textarea('description', __('Description'));
        $form->select('learning_facility_id', __('Learning Facility'))
        ->options(LearningFacility::where('status', 1)->pluck('name', 'id'));
        //disable
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
}
