<?php

namespace App\Admin\Controllers;

use App\LearningShift;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LearningShiftAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Learning Shift';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LearningShift);

        $grid->column('id', __('ID'));
        $grid->column('title', __('Title'));
        $grid->column('from_time', 'Bắt đầu (h)');
        $grid->column('to_time', 'Kết thúc (h)');
        $grid->column('description', __('Description'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->expand();
            $filter->disableIdFilter();
            $filter->like('title', 'Title');
        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data learning shift</div>";
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LearningShift::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', __('Title'));
        $show->field('from_time', __('From time'));
        $show->field('to_time', __('To time'));
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        for($i = 1; $i <= 24; $i ++) {
            $time[$i] = $i; 
        }
        $form = new Form(new LearningShift);

        $form->text('title', __('Title'));
        $form->select('from_time')->options($time);
        $form->select('to_time')->options($time);
        $form->textarea('description', __('Description'));
        //disable
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
}
