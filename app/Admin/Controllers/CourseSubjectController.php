<?php

namespace App\Admin\Controllers;

use App\CourseSubjectTemp;
use App\Course;
use App\Subject;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CourseSubjectController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Course_Subject';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CourseSubjectTemp);
        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('course_id', __('Course id'))->display(function($id){
            if(isset($id)) {
                return "<a href='courses'>".Course::find($id)->title."</a>";
            }
            else return "N/A";
        });
        $grid->column('subject_id', __('Subject id'))->display(function($id){
            if(isset($id)) {
                return "<a href='subjects'>".subject::find($id)->name."</a>";
            }
            else return "N/A";
        });

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function ($filter){
           $filter->equal('course_id', 'Tên khoá học')
               ->select(Course::all()->pluck('title', 'id'));
            $filter->equal('subject_id', 'Tên môn học')
               ->select(Subject::all()->pluck('name', 'id'));
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CourseSubjectTemp::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('course_id', __('Course id'))->as(function($id){
            if(isset($id)) {
                return Course::find($id)->title;
            }
            else return "N/A";
        });
        $show->field('subject_id', __('Subject id'))->as(function($id){
            if(isset($id)) {
                return Course::find($id)->title;
            }
            else return "N/A";
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CourseSubjectTemp);

        $form->select('course_id')->options(Course::all()->pluck('title','id'));
        $form->select('subject_id')->options(Subject::all()->pluck('name','id'));

        return $form;
    }
}
