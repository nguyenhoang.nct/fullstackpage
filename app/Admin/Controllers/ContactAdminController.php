<?php

namespace App\Admin\Controllers;

use App\Contact;
use App\RegisterCourse;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Admin;
use Illuminate\Support\Facades\DB;

class ContactAdminController extends BaseController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Contacts';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {   

        $grid = new Grid(new Contact);
        Admin::script("");
        $grid->header(function (){
            if(session('thongbao'))
                return "<div class='alert alert-success'>".session('thongbao')."</div>";
        });
        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('full_name', __('Full name'));
        $grid->column('phone', 'Phone');
//        $grid->column('email', __('Email'))->display(function ($email){
//            return "<a href='mailto:" .$email."'>$email</a>";
//        });
//        $grid->column('address', __('Address'));
//        $grid->column('title', __('Title'));
//        $grid->column('message', __('Message'));
        $grid->column('status', 'Status')->editable('select', Contact::STATUS);

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
//        $grid->column('Convert To Leads')->display(function(){
//                return "<a href='convert-to-leads/$this->id/contacts'>Convert To Leads</a>";
//        });

        $grid->filter(function ($filter){
           $filter->expand();
           $filter->disableIdFilter();

           $filter->like('full_name', 'Full name');
           $filter->equal('phone', 'Phone');
           $filter->equal('status')->radio(Contact::STATUS);
           $filter->between('created_at', 'Created_at')->datetime();
        });
        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data liên hệ</div>";
        });
//        $grid->actions(function ($action){
//           $action->disableDelete();
//           $action->disableEdit();
//        });
        $grid->disableCreateButton();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Contact::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('full_name', __('Full name'));
        $show->field('phone', 'Phone');
        $show->status('Status')->as(function ($status){
            if($status == 0)
                return 'Chưa học thử';
            else
                return 'Đã học thử';
        });
//        $show->field('email', __('Email'));
//        $show->field('address', __('Address'));
//        $show->field('title', __('Title'));
//        $show->field('message', __('Message'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Contact);

        $form->text('full_name', __('Full name'));
        $form->text('phone', 'Phone');
        $form->select('status')->options(Contact::STATUS);
//        $form->email('email', __('Email'));
//        $form->textarea('address', __('Address'));
//        $form->text('title', __('Title'));
//        $form->textarea('message', __('Message'));

        return $form;
    }
    public function convertToLeads($id, $table) {
        $flag = false;
        switch ($table) {
            case 'contacts':
                $data = Contact::find($id);
                if(isset($data)) {
                   self::InsertTableUser($id, $data->full_name, $data->email, $data->address, $data->phone);
                    DB::table('contacts')->where('id', $id)
                        ->update(
                            ['status' => 1]
                        );
                   $flag = true;
                }
                break;
            case 'register_courses':
                $data = RegisterCourse::find($id);
                    if(isset($data)) {
                        self::InsertTableUser($id, $data->full_name, $data->email, $data->address, $data->phone, $data->linkfb);
                        DB::table('register_courses')->where('id', $id)
                            ->update(
                                ['status' => 1]
                            );
                        $flag = true;
                    }
                break;
            default:
                $flag = false;
                break;
        }
        if($flag == true) return redirect()->back()->with('thongbao', 'Update!');
        else return redirect()->back()->with('thongbao', 'Something Went Wrong !');
    }
}
