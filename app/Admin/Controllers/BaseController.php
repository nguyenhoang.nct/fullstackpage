<?php

namespace App\Admin\Controllers;

use Encore\Admin\Controllers\AdminController;
use Illuminate\Support\Facades\DB;

class BaseController extends AdminController
{

    const ENABLE = 1;
    const DISABLE = 0;
    public function InsertTableUser($id, $full_name, $email, $address, $phone, $facebook = '')
    {
        $password = bcrypt('fullstack');
        DB::table('users')
            ->insert(
                [
                    'register_course_id' => $id,
                    'name'     => $full_name,
                    'email'    => $email,
                    'password' => $password,
                    'address'  => $address,
                    'phone'    => $phone,
                    'type'     => 0,
                    'facebook' => $facebook
                ]
            );
    }
}
