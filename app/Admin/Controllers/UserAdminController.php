<?php

namespace App\Admin\Controllers;

use App\User;
use App\Classes;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Support\Facades\DB;
use App\RollUp;
use Illuminate\Http\Request;

class UserAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'User';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('name', __('Name'));
        $grid->column('email', __('Email'));
        //$grid->column('email_verified_at', __('Email verified at'));
        //$grid->column('password', __('Password'));
        //$grid->column('remember_token', __('Remember token'));
       
        $grid->column('class_id', __('Class'))
            ->editable('select', Classes::all()->pluck('title', 'id'));

        $grid->column('type', __('Type'))->display(function($id){
            if(isset($id)) return User::TYPE[$id];
            else return "N/A";
        });
        $grid->column('phone', __('Phone'));
        $grid->column('address', __('Address'));
        $grid->column('facebook', __('Facebook'))->display(function(){
            $link = User::select('facebook')->where('id', '=', $this->id)->first();
            return "<a href=$link->facebook>$link->facebook</a>";
        });
        $grid->column('register_course_id', 'Register Course ID')->display(function ($register_course_id){
            return "<a href='/admin/registercourses/$register_course_id'>$register_course_id</a>";
        });
        //$grid->column('status', __('Status'))->editable('select', User::STATUS);
        $grid->column('note', __('Note'));

        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function ($filter){
            $filter->column(1/2, function ($filter){
                $filter->disableIdFilter();
                $filter->like('name', 'Nhập Tên');
                $filter->like('email', 'Nhập email');
                $filter->like('phone', 'Nhập Sdt');
                $filter->like('address', 'Nhập địa chỉ');
                $filter->like('name', 'Nhập facebook');
            });

            $filter->column(1/2, function ($filter){
               $filter->equal('status', 'Status')->radio(User::STATUS);
               $filter->equal('class_id', 'Class')->select(Classes::all()->pluck('title','id'));
               $filter->equal('type', 'Type')->select(User::TYPE);
               $filter->between('created_at', 'Ngày tạo')->datetime();
               $filter->between('update_at', 'Ngày update')->datetime();

            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('email', __('Email'));
        $show->field('email_verified_at', __('Email verified at'));
        $show->field('password', __('Password'));
        $show->field('remember_token', __('Remember token'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));
        $show->field('class_id', __('Class id'));
        $show->field('type', __('Type'));
        $show->field('phone', __('Phone'));
        $show->field('address', __('Address'));
        $show->field('facebook', __('Facebook'));
        $show->field('status', __('Status'));
        $show->field('note', __('Note'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('name', __('Name'));
        $form->email('email', __('Email'));
        //$form->datetime('email_verified_at', __('Email verified at'))->default(date('Y-m-d H:i:s'));
        //$form->password('password', __('Password'));
        //$form->text('remember_token', __('Remember token'));
        $form->select('class_id', __('Classes'))->options(Classes::all()->pluck('title','id'));
        $form->select('type', __('Type'))->options(User::TYPE);
        $form->mobile('phone', __('Phone'));
        $form->textarea('address', __('Address'));
        $form->text('facebook', __('Facebook'));
        $form->radio('status', __('Status'))->options(Classes::STATUS)->default(1);
        $form->textarea('note', __('Note'));
         // mã hoá
        $form->saving(function(Form $form) {
            if($form->password && $form->model()->password != $form->password)
            {
                $form->password = bcrypt($form->password);
            }
        });

        return $form;
    }
    
}
