<?php

namespace App\Admin\Controllers;

use App\Code;
use App\Course;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CodeAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'App\Code';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Code);

        $grid->column('id', __('ID'));
        $grid->column('code', __('Code'));
        $grid->column('course_id', 'Course')->display(function ($course){
//            $title_course = Course::where('id', $course)
//                ->select('title')->first();
            return isset($course) ? (Course::where('id', $course)
                ->select('title')->first())->title : 'Dành cho tất cả các khóa';
        });
        $grid->column('status', __('Status'))->editable('select', Code::STATUS);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->expand();
            $filter->disableIdFilter();
            $filter->equal('code', 'Code');
            $filter->equal('course_id', 'Course')->select(Course::where('status', 1)->pluck('title', 'id'));
            $filter->scope('course_id', 'Dành cho tất cả khóa học')->where('course_id', null);
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Code::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('code', __('Code'));
        $show->field('course_id', 'Course')->as(function ($course){
            return isset($course) ? (Course::where('id', $course)
                ->select('title')->first())->title : 'Dành cho tất cả các khóa';
        });
        $show->field('status', __('Status'))->as(function ($status){
            if($status == 1)
            {
                return 'Chưa sử dụng';
            }
            else
            {
                return 'Đã sử dụng';
            }
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Code);

        $form->text('code', __('Code'));
        $form->select('course_id', 'Course')
            ->options(Course::where('status', 1)->pluck('title', 'id'));
        $form->switch('status', __('Status'))->default(1);

        return $form;
    }
}
