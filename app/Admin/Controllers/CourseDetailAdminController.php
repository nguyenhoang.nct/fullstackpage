<?php

namespace App\Admin\Controllers;

use App\Course;
use App\CourseDetail;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class CourseDetailAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Course Detail';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new CourseDetail);

        $grid->column('id', __('ID'));
        $grid->course()->title('Course');
        $grid->column('images', __('Images'))->image();
        $grid->column('content', __('Content'))->display(function ($des){
            return htmlspecialchars_decode($des);
        });
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->filter(function ($filter){
            $filter->expand();
            $filter->disableIdFilter();

            $filter->equal('course_id', 'Course')->select(Course::all()->pluck('title', 'id'));
            $filter->equal('created_at', 'Created_at')->datetime();
        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data course detail</div>";
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(CourseDetail::findOrFail($id));

        $show->field('id', __('ID'));
        $show->course()->title('Course');
        $show->field('images', __('Images'))->image();
        $show->field('content', __('Content'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new CourseDetail);

        $form->select('course_id', __('Course'))
            ->options(Course::all()->pluck('title', 'id'));
        $form->multipleImage('images', __('Images'), function ($images){
            return setImagesAttribute($images);
        })->move('courses/images')->removable();
        $form->ckeditor('content', __('Content'));

        return $form;
    }
}
