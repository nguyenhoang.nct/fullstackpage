<?php

namespace App\Admin\Controllers;

use App\Logo;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class LogoAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Logos';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Logo);

        $grid->column('id', __('ID'));
        $grid->column('image', __('Image'))->image();
        $grid->column('url', __('Url'));
        $grid->column('status', __('Status'))->editable('select', Logo::STATUS);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data logos</div>";
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Logo::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('image', __('Image'))->image();
        $show->field('url', __('Url'));
        $show->field('status', __('Status'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Logo);

        $form->image('image', __('Image'))->rules('required')->move('logos')
        ->removable();
        $form->text('url', __('Url'));
        $form->switch('status', __('Status'))->default(1);

        return $form;
    }
}
