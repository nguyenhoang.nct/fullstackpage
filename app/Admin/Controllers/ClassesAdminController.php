<?php

namespace App\Admin\Controllers;

use App\Classes;
use App\Classroom;
use App\Course;
use App\LearningShift;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class ClassesAdminController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Classes';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Classes);
        
        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('title', __('Title'));
        $grid->column('course_id', __('Course'))->display(function($id){
            return isset($id) ? Course::find($id)->title : "N/A";
            
        });
        $grid->column('classroom_id', 'Classroom')->display(function($id){
            if(isset($id)) return Classroom::find($id)->title;
            else return "N/A";
        });
        $grid->column('learning_shift_id', 'Learning Shift')->display(function($id){
            if(isset($id)) return LearningShift::find($id)->title;
            else return "N/A";
        });
        $grid->column('start_learning_date', 'Start');
        $grid->column('end_learning_date', 'End');
        $grid->column('day', 'Day');
        $grid->column('user_id', 'Teacher')->display(function($id){
            if(isset($id)) return User::find($id)->name;
            else return "N/A";
        });
        //$grid->column('status', __('Status'))->editable('select', Classes::STATUS);
        $grid->column('description', __('Description'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function($filter){
            $filter->expand();
            $filter->disableIdFilter();

            $filter->column(1/2, function ($filter){
                $filter->equal('title', 'Title')->placeholder('Nhập tên lớp học');
                $filter->equal('course_id', 'Course')
                    ->select(Course::where('status', 1)->pluck('title','id'));
                $filter->equal('classroom_id', 'Classroom')
                    ->select(Classroom::all()->pluck('title', 'id'));
                $filter->equal('learning_shift_id', 'Learning Shift')
                    ->select(LearningShift::all()->pluck('title', 'id'));
            });

            $filter->column(1/2, function ($filter){
                $filter->date('start_learning_date', 'Start');
                $filter->date('end_learning_date', 'End');
                //$filter->equal('status', 'Status')->radio(Classes::STATUS);
                $filter->between('created_at', 'Created_at')->datetime();
                //$filter->between('updated_at', 'Updated_at')->datetime();
            });

        });

        $grid->footer(function ($query){
            $data = $query->count('id');
            return "<div style='padding: 10px;' class='btn btn-success'>Tổng có : $data classes</div>";
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Classes::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('title', __('Title'));
        $show->field('course_id', __('Course'))->as(function($id){
            if(isset($id)) return Course::find($id)->title;
            else return "N/A";
        });
        $show->field('classroom_id', 'Classroom')->as(function($id){
            if(isset($id)) return Course::find($id)->title;
            else return "N/A";
        });
        $show->field('learning_shift_id', 'Learning Shift')->as(function($id){
            if(isset($id)) return LearningShift::find($id)->title;
            else return "N/A";
        });
        $show->field('start_learning_date', 'Start');
        $show->field('end_learning_date', 'End');
        $show->field('day', 'Day');
        //$show->field('status', __('Status'));
        $show->field('description', __('Description'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Classes);

        $form->text('title', __('Title'))->rules('required');
        $form->select('course_id', __('Course'))->options(Course::where('status', 1)->pluck('title','id'))->rules('required');
        $form->select('classroom_id', 'Classroom')->options([])->ajax('/admin/api/load-branches')->rules('required');
        // $form->select('learning_shift_id', 'Learning Shift')->options(LearningShift::all()->pluck('title', 'id'))->rules('required');

        $form->select('learning_shift_id', 'Learning Shift')->options([])->ajax('/admin/api/load-learning-shift')->rules('required');
        $form->date('start_learning_date', 'Start')->rules('required');
        $form->date('end_learning_date', 'End')->rules('required');
        $form->tags('day', 'Day')->options(Classes::DAY)->rules('required');
        $teacher = array();
        $data = User::where('type', 2)->get();
        foreach($data as $dt) {
            $teacher[$dt->id] = $dt->name;
        }
        $form->select('user_id', 'Select teacher')->options($teacher);
        //$form->radio('status', __('Status'))->options(Classes::STATUS)->default(1);;
        $form->textarea('description', __('Description'));
        $form->saving(function (Form $form) {
            $start_time = strtotime($form->start_learning_date);
            $end_time   = strtotime($form->end_learning_date); 
            // format day->string;
            $day = array();
            for($i = 0; $i < count ($form->day) - 1; $i++) {
                $day[$i] = $form->day[$i];
            }
            $day = (implode(",",$day));
            ///////////////////////////
            foreach (Classes::all() as $value) {
                // id phòng, id ca, thứ trùng nhau thì kiểm tra thời gian có trùng nhau k
                if($value->classroom_id == $form->classroom_id && $value->learning_shift_id == $form->learning_shift_id && $value->day == $day) {
                    // check ko trùng thì cho insert
                    if($start_time > strtotime($value->end_learning_date) || $end_time < strtotime($value->start_learning_date)) {
                        $flag = true;
                    }
                    // ngược lại trùng nhau thì ko cho insert
                    else {

                        $flag = false;
                        break;
                    }
                }
                else $flag = true;
            }
            if($flag == true) {
                admin_toastr('Success..', 'success', ['timeOut' => 5000]);
            }
            if($flag == false) {
                admin_toastr('Thời gian bị trùng, vui lòng chọn lại', 'error', ['timeOut' => 5000]);
                return redirect()->back();
                
            }
        });

        //disable
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
    public function loadLearningShift(Request $request) {
        $q = $request->get('q');
        return LearningShift::select('id', 'title', 'from_time', 'to_time')
        ->addSelect(DB::raw("CONCAT('Title : ',title,' - Bắt đầu : ',from_time, 'h Kết thúc: ', to_time,'h') as text"))

            ->where(function($query) use ($q){
                $query->where('title', 'like', '%'.$q.'%')
                    ->orWhere('from_time', 'like', '%'.$q.'%')
                    ->orWhere('to_time', 'like', '%'.$q.'%');
                    
            })->paginate(null);
    }
    public function loadBranches(Request $request) {
        $q = $request->get('q');
        return Classroom::select('classrooms.id', 'classrooms.title', 'learning_facilities.name', 'learning_facilities.address')
        ->addSelect(DB::raw("CONCAT('Tên Phòng : ',title,' - Chi Nhánh : ',name,' - Địa Chỉ : ',address) as text"))
            ->join('learning_facilities', 'learning_facilities.id', '=', 'classrooms.learning_facility_id')
            ->where(function($query) use ($q){
                $query->where('title', 'like', '%'.$q.'%')
                    ->orWhere('name', 'like', '%'.$q.'%')
                    ->orWhere('address', 'like', '%'.$q.'%');
                    
            })->paginate(null);
    }
}
