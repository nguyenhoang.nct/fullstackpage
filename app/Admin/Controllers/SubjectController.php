<?php

namespace App\Admin\Controllers;

use App\Subject;
use App\Course;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class SubjectController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Subject';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Subject);

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('image', __('Image'));
        $grid->column('description', __('Description'));
        $grid->column('number_session', __('Number session'));
        $grid->column('price', __('Price'));
        $grid->column('promotion_price', __('Promotion price'));
        $grid->column('status', __('Status'))->editable('select', Subject::STATUS);
        $grid->column('is_free')->editable('select', Subject::STATUS);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function ($filter){
           $filter->column(1/2, function ($filter){
                $filter->like('name');
                $filter->like('description');
                $filter->like('number_session');


            });
            
            $filter->column(1/2, function ($filter){
                $filter->like('price');
                $filter->like('promotion_price');
                 $filter->equal('status')->radio(Subject::STATUS);
                $filter->between('created_at', 'Created_at')->datetime();
                
            });
        });
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Subject::findOrFail($id));

        $show->field('id', __('Id'));
        $show->field('course_id', __('Course id'))->as(function($course_id){
            if(isset($course_id)) {
                return Course::find($course_id)->title;
            }
            else return "N/A";
        });
        $show->field('name', __('Name'));
        $show->field('image', __('Image'));
        $show->field('description', __('Description'));
        $show->field('number_session', __('Number session'));
        $show->field('price', __('Price'));
        $show->field('promotion_price', __('Promotion price'));
        $show->field('status', __('Status'));
        $show->field('is_free', __('Is free'));
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Subject);

        $form->number('course_id', __('Course id'));
        $form->text('name', __('Name'));
        $form->image('image', __('Image'));
        $form->textarea('description', __('Description'));
        $form->text('number_session', __('Number session'));
        $form->text('price', __('Price'));
        $form->text('promotion_price', __('Promotion price'));
        $form->number('status', __('Status'));
        $form->switch('is_free', __('Is Free'))->default(0);

        return $form;
    }
}
