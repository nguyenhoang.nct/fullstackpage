<?php

namespace App\Admin\Controllers;

use App\LearningFacility;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
Use Encore\Admin\Widgets\Table;

class LearningFacilityController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Branches';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new LearningFacility);

        $grid->column('id', __('ID'))->sortable();
        $grid->model()->orderBy('id', 'desc');
        $grid->column('name', __('Name'))->expand(function ($model) {
            $class_room = $model->classRoom->map(function ($class_room) {
                return $class_room->only(['id', 'title', 'description']);
            });
            return new Table(['ID', 'Title', 'Description'], $class_room->toArray());
        });
        $grid->column('address', __('Address'));
        $grid->column('hotline', __('Hotline'));
        $grid->column('note', __('Note'));
        $grid->column('status', __('Status'))->editable('select', LearningFacility::STATUS);
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));
        $grid->filter(function ($filter){
            $filter->column(1/2, function ($filter){
                $filter->disableIdFilter();
                $filter->like('name', 'Nhập Tên');
                $filter->like('address', 'Nhập địa chỉ');
                $filter->equal('hotline', 'Nhập sdt');
            });
            $filter->column(1/2, function ($filter){
                $filter->disableIdFilter();
                $filter->equal('note', 'Note');
                $filter->equal('status', 'Status')->radio(LearningFacility::STATUS);
            });
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(LearningFacility::findOrFail($id));

        $show->field('id', __('ID'));
        $show->field('name', __('Name'));
        $show->field('address', __('Address'));
        $show->field('hotline', __('Hotline'));
        $show->field('note', __('Note'));
        $show->field('status', __('Status'))->as(function($status){
            return $status == 1 ? "Enable" : "Disable";
        });
        $show->field('created_at', __('Created at'));
        $show->field('updated_at', __('Updated at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new LearningFacility);

        $form->text('name', __('Name'));
        $form->textarea('address', __('Address'));
        $form->text('hotline', __('Hotline'));
        $form->textarea('note', __('Note'));
        $form->select('status', __('Status'))->options(LearningFacility::STATUS)->default(1);
        //disable
        $form->disableEditingCheck();
        $form->disableCreatingCheck();
        $form->disableViewCheck();
        return $form;
    }
}
