<?php

use App\Libs\RouteLib;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('admin.home');

    $router->resource('contacts', ContactAdminController::class);
    $router->resource('courses', CourseAdminController::class);
    $router->resource('coursedetails', CourseDetailAdminController::class);
    $router->resource('documents', DocumentAdminController::class);
    $router->resource('logos', LogoAdminController::class);
    $router->resource('registercourses', RegisterCourseAdminController::class);
    $router->resource('settings', SettingAdminController::class);
    $router->resource('students', StudentAdminController::class);
    $router->resource('teachers', TeacherAdminController::class);
    $router->resource('codes', CodeAdminController::class);
    $router->resource('learning-facilities', LearningFacilityController::class);
    $router->resource('users', UserAdminController::class);
    $router->resource('classes', ClassesAdminController::class);
    $router->resource('classrooms', ClassRoomAdminController::class);
    $router->resource('learning-shifts', LearningShiftAdminController::class);
    $router->get('convert-to-leads/{id}/{table}', 'ContactAdminController@convertToLeads');
    $router->group([
        'prefix' => 'api',
    ], function (Router $router) {
        $router->get('load-learning-shift','ClassesAdminController@loadLearningShift');
        $router->get('load-branches','ClassesAdminController@loadBranches');
    });
    $router->resource('subjects', SubjectController::class);
    $router->resource('course-subject-temps', CourseSubjectController::class);

    RouteLib::generateRoute('blogs', '\App\Http\Controllers\Admin\BlogController', 'blog');

    RouteLib::generateRoute('blog-categories', '\App\Http\Controllers\Admin\BlogCategoryController', 'blog_category');

    RouteLib::generateRoute('blog-tags', '\App\Http\Controllers\Admin\BlogTagController', 'blog_tag');

    RouteLib::generateRoute('blog-searches', '\App\Http\Controllers\Admin\BlogSearchController', 'blog_search');

    $router->get('/blog-searches/import', '\App\Http\Controllers\Admin\BlogSearchController@import')->name('admin.blog_search.import');
    $router->post('/blog-searches/import', '\App\Http\Controllers\Admin\BlogSearchController@importPost')->name('admin.blog_search.import_post');


});
