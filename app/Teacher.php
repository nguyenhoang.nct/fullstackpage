<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
}
