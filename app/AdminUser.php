<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    //
    protected $table = 'admin_users';
    public function leads() {
    	return $this->hasMany('App\Leads', 'admin_user_id', 'id');
    }

}
