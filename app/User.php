<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
     CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
    CONST TYPE = [
        0 => 'Leads',
        1 => 'Students',
        2 => 'Teacher'
    ];
    public function attendance() {
        return $this->belongsTo('App\Attendance', 'user_id', 'id');
    }

}

