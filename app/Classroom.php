<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
    //
    protected $table = 'classrooms';
    public function learningFacility() {
    	return $this->belongsTo('App\LearningFacility', 'learning_facility_id', 'id');
    }
    public function classes() {
    	return $this->hasMany('App\Classes', 'classroom_id', 'id');
    }
}
