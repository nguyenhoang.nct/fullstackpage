<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegisterCourse extends Model
{
    protected $table = 'register_courses';

    protected $fillable = [
        'full_name', 'phone', 'email', 'course_id','birthday','address','job','note', 'linkfb'
    ];

    const STATUS = [
      1 => 'Đã học thử',
      0 => 'Chưa học thử',
    ];

    public function courses()
    {
        return $this->hasMany('App\Course', 'course_id', 'id');
    }
}
