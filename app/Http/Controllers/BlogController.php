<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Libs\ElasticBlogLib;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogSearch;
use App\Models\BlogTag;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class BlogController extends Controller
{
    static $_ACTIVE = 1;
    /**
     * Blog index page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $newestPost = Blog::getHomeNewestPost();
        $newestPostID = [];
        if (!empty($newestPost)) {
            foreach ($newestPost as $key => $value) {
                $newestPostID[] = $value->id;
            }
        }

        $category = BlogCategory::getAll();

        $postByCategory = Blog::getHomePostByCategory($category, $newestPostID);

        SEOTools::setTitle('Tin tức về công nghệ, lập trình và đào tạo lập trình mới nhất');

        return view('blog.index', compact('newestPost', 'category', 'postByCategory'));
    }


    /**
     * Blog category page
     *
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category($alias)
    {
        $category = BlogCategory::where('alias', $alias)->first();

        if (empty($category)) {
            abort(404);
        }

        $listPost = Blog::where('category_id', $category->id)
            ->with('category')
            ->orderBy('id', 'DESC')
            ->paginate(20);

        SEOTools::setTitle($category->name);
        SEOTools::setDescription($category->meta_description);
        SEOTools::opengraph()->addImage(asset($category->image));


        return view('blog.category', compact('category', 'listPost'));
    }

    /**
     * Tag page
     *
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tag($alias)
    {
        $tag = BlogTag::where('alias', $alias)->first();

        if (empty($tag)) {
            Log::error('Not found | Tag | Alias: '.$alias);
            abort(404);
        }

        $listPostId = BlogTag::where('alias', $alias)->pluck('blog_id')->toArray();

        $listPost = Blog::whereIn('id', $listPostId)
            ->with('category')
            ->orderBy('id', 'DESC')
            ->paginate(15);

        if($listPost->isEmpty()){
            Log::error('Not found Post | | Tag | Alias: '.$alias);
        }

        $searchKeyword = BlogSearch::where('alias', $alias)->get();

        SEOTools::setTitle($tag->name);

        return view('blog.tag', compact('listPost', 'tag', 'searchKeyword'));
    }


    /**
     * Function search keyword
     *
     * @param $alias
     * @param int $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search($alias, $page = 1)
    {
        $page = (int)($page) > 1 ? (int)($page) : 1;

        $search = BlogSearch::where('alias', $alias)->first();

        if (empty($search)) {
            $search = BlogSearch::create([
                'keyword' => str_replace('-', ' ', $alias),
                'alias' => $alias,
                'alias' => '0'
            ]);
        }

        $route = route('web.blog.search', ['keyword' => $alias, 'page' => $page]);

        $listPost = ElasticBlogLib::getPaginateByQuery('title:' . $search->keyword, $page, $route);

        $SEOTitle = $search->keyword;
        if($page > 1){
            $SEOTitle .= ' - Trang '.$page;
        }
        SEOTools::setTitle($SEOTitle);

        if (!$listPost->isEmpty()) {
            foreach($listPost as $key => $value){
                if(!empty($value->description)){
                    SEOTools::setDescription($value->description);
                    break;
                }
            }

        }

        return view('blog.search', compact('listPost', 'search'));
    }


    /**
     * Detail post
     *
     * @param $id
     * @param $alias
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detail($id, $alias)
    {
        $post = Cache::remember('Blog::detail::' . $id , 360, function () use ($id) {
            $post = Blog::where('id', $id)
                ->where('status', self::$_ACTIVE)
                ->with('category')
                ->first();

            return $post;
        });

        $listTag = Cache::remember('Blog::detail::tagList' . $id, 360, function () use ($id) {
            $listTag = BlogTag::where('blog_id', $id)->get();
            return $listTag;
        });
        /**
         * Lấy tin cùng chuyên mục:
         * 3 bài trước và 3 bài sau
         */
        $categoryId = $post->category_id;
        $type = $post->type;
        $sameCategory = Cache::remember('Blog::detail::same_category::' . $id, 3600, function () use ($id, $categoryId, $type) {
            $prePost = Blog::where('category_id', $categoryId)
                ->where('type', $type)
                ->where('id', '<', $id)
                ->orderBy('id', 'DESC')
                ->limit(3)->get();

            $nextPost = Blog::where('category_id', $categoryId)
                ->where('type', $type)
                ->where('id', '>', $id)
                ->orderBy('id', 'ASC')
                ->limit(3)->get();

            $listPost = [];
            if(!$prePost->isEmpty()){
                foreach($prePost as $key => $value){
                    $listPost[] = $value;
                }
            }
            if(!$nextPost->isEmpty()){
                foreach($nextPost as $key => $value){
                    $listPost[] = $value;
                }
            }

            return $listPost;
        });

        /**
         * Lấy tin liên quan
         * 12 bài có từ khóa tương tự nhất
         * Lấy từ elastic search
         */
        $relatePost = ElasticBlogLib::getByQuery('title:' . $post->title);

        SEOTools::setTitle($post->title);
        SEOTools::setDescription($post->meta_description);
        SEOTools::opengraph()->addImage(asset($post->images));

        return view('blog.detail', compact('post', 'relatePost', 'listTag', 'sameCategory'));
    }
}
