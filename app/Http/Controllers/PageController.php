<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Course;
use App\CourseSubjectTemp;
use App\Logo;
use App\RegisterCourse;
use App\Setting;
use App\Student;
use App\Subject;
use App\Teacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PageController extends Controller
{

    public function getIndex()
    {

        $courses = Course::where('status', 1)->where('is_free', 0)->get();

        $students = Student::where('status', 1)->take(3)->get();

        $teachers = Teacher::where('status', 1)->take(3)->get();

        foreach($courses as  $key => $course) {
            $courses[$key]->alias = $this->convertName($course->title);
        }

        return view('landing.page.index', compact('teachers', 'students', 'courses'));

    }

    public function postContact(Request $request)
    {

        $validator = Validator::make($request->all(),

            [

                'name' => 'required',

                'phone' => 'required',

            ],

            [

                'name.required' => 'Trường này không được để trống ',

                'phone.required' => 'Trường này không được để trống ',

            ]);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'message' => $validator->errors()], 200);

        } else {

            Contact::create([

                'full_name' => $request->name,

                'phone' => $request->phone,

                'status' => 0,

            ]);

            return response()->json(['success' => true, 'message' => 'Đăng ký thành công ! Chúng tôi sẽ liên lạc lại với bạn !'], 200);

        }

    }

    public function postRegister(Request $request)
    {

        $validator = Validator::make($request->all(),

            [
                'register_name' => 'required',
                'register_email' => 'required',
                'register_phone' => 'required',
                'register_course' => 'required',
                'register_birthday' => 'required',
                'register_address' => 'required',
            ],

            [
                'register_name.required' => 'Xin vui lòng điền họ tên',
                'register_email.required' => 'Email không được bỏ trống',
                'register_phone.required' => 'SĐT không được bỏ trống',
                'register_course.required' => 'Xin vui lòng chọn khóa học cần đăng ký',
                'register_birthday.required' => 'Ngày sinh không được bỏ trống',
                'register_address.required' => 'Địa chỉ không được bỏ trống',

            ]);

        if ($validator->fails()) {

            return response()->json(['success' => false, 'message' => $validator->errors()], 200);

        } else {

            RegisterCourse::create([

                'full_name' => $request->register_name,

                'email' => $request->register_email,

                'phone' => $request->register_phone,

                'course_id' => $request->register_course,

                'birthday' => $request->register_birthday,

                'address' => $request->register_address,

                'job' => $request->register_job,

                'note' => $request->register_note,

                'linkfb' => $request->link_fb,

            ]);

            return response()->json(['success' => true, 'message' => 'Đăng ký thành công !'], 200);

        }

    }

    // public function getDetailSubject($id)
    // {
    //     $subject_detail = Subject::where('course_id', $id)->where('status', 1)->get();
    //     $course_name = Course::find($id);
    //     return response()->json(['subject_detail' => $subject_detail, 'course_name' => $course_name], 200);
    // }

    public function register(Request $request)
    {
        $courses = Course::where('status', self::ENABLE)
            ->where('is_free', self::DISABLE)->get();

        $defaultCourse = $request->query('c');

        return view('landing.page.register_page', [
            'courses' => $courses,
            'defaultCourse' => $defaultCourse,
        ]);

    }

    public function registerFree()
    {

        $courses_free = Course::where([

            ['status', '=', self::ENABLE],

            ['is_free', '=', self::ENABLE],

        ])->get();

        return view('landing.page.register_free', ['courses_free' => $courses_free]);

    }

    public function getDetailCourse($id) {
        $course = Course::where('id', $id)->first();
        $courses = Course::where('status', 1)->where('is_free', 0)->get();

        foreach($courses as  $key => $coursex) {
            $courses[$key]->alias = $this->convertName($coursex->title);
        }
        return view('landing.page.course_detail', ['course' => $course, 'courses' => $courses]);
    }

    public function getDetailSubject($id)
    {
        $course_name = Course::where('id', $id)->first();
        $subject = CourseSubjectTemp::where('course_id', $id)->get();
        $arr_name = $arr_description = $arr_number_session = $arr_price = $arr_promotion_price = array();
        foreach ($subject as $value) {
            //echo $value->subject_id."<br>";
            $data_subject = Subject::where('id', $value->subject_id)->where('status', 1)->get();
            foreach ($data_subject as $data) {
                $arr_name[] = $data->name;
                $arr_description[] = $data->description;
                $arr_number_session[] = $data->number_session;
                $arr_price[] = $data->price;
                $arr_promotion_price[] = $data->promotion_price;
            }
        }

        return response()->json(['course_name' => $course_name, 'subject_detail' => ['name' => $arr_name, 'description' => $arr_description, 'number_session' => $arr_number_session, 'price' => $arr_price, 'promotion_price' => $arr_promotion_price]], 200);

    }
    function convertName($str) {
		$str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
		$str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
		$str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
		$str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
		$str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
		$str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
		$str = preg_replace("/(đ)/", 'd', $str);
		$str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
		$str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
		$str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
		$str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
		$str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
		$str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
		$str = preg_replace("/(Đ)/", 'D', $str);
		$str = preg_replace("/(\“|\”|\‘|\’|\,|\!|\&|\;|\@|\#|\%|\~|\`|\=|\_|\'|\]|\[|\}|\{|\)|\(|\+|\^)/", '-', $str);
		$str = preg_replace("/( )/", '-', $str);
		return $str;
	}

}
