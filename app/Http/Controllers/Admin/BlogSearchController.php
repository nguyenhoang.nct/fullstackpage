<?php
namespace App\Http\Controllers\Admin;

use App\Models\BlogCategory;
use App\Models\BlogSearch;
use App\Models\BlogTag;
use Illuminate\Http\Request;

class BlogSearchController extends ResourceController
{
    protected $model = BlogSearch::class;
    protected $viewPath = 'blog_search';
    protected $name = 'Blog Search';
    protected $route = 'blog_search';

    /**
     * Update category page
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->updateData = [
            'keyword' => $request->parent_id,
            'alias' => $request->name
        ];
        return parent::update($request, $id);
    }

    public function import()
    {
        return view('admin.blog_search.import');
    }

    /**
     * Import batch keyword
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function importPost(Request $request)
    {
        $this->validate($request, [
            'list_keyword' => 'required'
        ]);
        $listKeyword = $request->list_keyword;
        $listKeyword = explode("\r\n", $listKeyword);

        $blogSearch = BlogSearch::pluck('alias')->toArray();

        $listInsert = [];
        if(!empty($listKeyword)){
            foreach($listKeyword as $keyword){
                $alias = str_slug($keyword);
                if(!in_array($alias, $blogSearch)){
                    $listInsert[] = [
                        'keyword' => $keyword,
                        'alias' => $alias
                    ];
                }
            }
        }

        if(!empty($listInsert)){
            BlogSearch::insert($listInsert);
        }

        return response()->json(['success' => 'Insert thành công', 'url' => route('admin.blog_search.index')]);
    }
}