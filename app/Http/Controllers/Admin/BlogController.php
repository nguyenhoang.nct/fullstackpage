<?php
namespace App\Http\Controllers\Admin;

use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogTag;
use Illuminate\Http\Request;

class BlogController extends ResourceController
{
    protected $model = Blog::class;
    protected $viewPath = 'blog';
    protected $name = 'Blog';
    protected $route = 'blog';

    public function index()
    {
        $this->data['listCategory'] = BlogCategory::pluck('name', 'id')->toArray();
        return parent::index();
    }

    public function create()
    {
        $this->data['listCategory'] = BlogCategory::pluck('name', 'id')->toArray();

        return parent::create();
    }

    public function edit($id)
    {
        $this->data['listCategory'] = BlogCategory::pluck('name', 'id')->toArray();

        return parent::edit($id);
    }

    public function update(Request $request, $id)
    {
        $post = $request->all();

        $this->updateData = [
            'category_id' => $request->category_id,
            'title' => $request->title,
            'alias' => $request->alias,
            'images' => $request->images,
            'description' => $request->description,
            'detail' => $request->detail,
            'meta_description' => $request->meta_description,
            'meta_keyword' => $request->meta_keyword
        ];

        // update tags
        $newTags = $post['tags'] ?? [];
        $oldTags = BlogTag::where('blog_id', $id)->pluck('name')->toArray();

        $deletedTag = array_diff($oldTags, $newTags);
        $insertNewTag = array_diff($newTags, $oldTags);

        if(!empty($deletedTag)){
            BlogTag::where('blog_id', $id)->whereIn('name', $deletedTag)->delete();
        }
        if(!empty($insertNewTag)){
            $insertTag = [];
            foreach($insertNewTag as $key => $value){
                $insertTag[] = [
                    'blog_id' => $id,
                    'name' => $value,
                    'alias' => str_slug($value)
                ];
            }
            BlogTag::insert($insertTag);
        }

        return parent::update($request, $id);
    }
}
