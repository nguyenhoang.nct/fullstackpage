<?php
namespace App\Http\Controllers\Admin;

use App\Models\BlogTag;
use Illuminate\Http\Request;

class BlogTagController extends ResourceController
{
    protected $model = BlogTag::class;
    protected $viewPath = 'blog_tag';
    protected $name = 'Blog Tag';
    protected $route = 'blog_tag';


    /**
     * Update category page
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->updateData = [
            'name' => $request->name,
            'alias' => $request->alias,
            'blog_id' => $request->blog_id
        ];
        return parent::update($request, $id);
    }
}