<?php
namespace App\Http\Controllers\Admin;

use App\Models\BlogCategory;
use App\Models\BlogTag;
use Illuminate\Http\Request;

class BlogCategoryController extends ResourceController
{
    protected $model = BlogCategory::class;
    protected $viewPath = 'blog_category';
    protected $name = 'Blog Category';
    protected $route = 'blog_category';

    /**
     * List category page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->data['listParent'] = BlogCategory::where('parent_id', 0)
            ->orWhereNull('parent_id')
            ->pluck('name', 'id')
            ->toArray();
        return parent::index();
    }


    /**
     * Create new category page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->data['listParent'] = BlogCategory::where('parent_id', 0)
            ->orWhereNull('parent_id')
            ->pluck('name', 'id')
            ->toArray();

        return parent::create();
    }


    /**
     * Edit category page
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data['listParent'] = BlogCategory::where('parent_id', 0)
            ->orWhereNull('parent_id')
            ->pluck('name', 'id')
            ->toArray();

        return parent::edit($id);
    }


    /**
     * Update category page
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->updateData = [
            'parent_id' => $request->parent_id,
            'name' => $request->name,
            'alias' => $request->alias,
            'image' => $request->image,
            'meta_description' => $request->meta_description,
            'meta_keyword' => $request->meta_keyword
        ];
        return parent::update($request, $id);
    }
}