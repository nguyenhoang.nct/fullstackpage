<?php
namespace App\Libs;

use App\Models\Blog;
use Elasticsearch\ClientBuilder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class ElasticBlogLib
{

    /**
     * Get blog from elastic search
     *
     * @param $query
     * @return Collection
     */
    public static function getByQuery($query)
    {
        $perPage = 15;

        $esClient = ClientBuilder::create()->build();
        try{
            $listPost = $esClient->search([
                'index' => env('ELASTICSEARCH_INDEX_NAME'),
                'size' => $perPage,
                'q' => $query
            ]);
        }catch (\Exception $ex){

        }

        $items = $listPost['hits']['hits'] ?? [];
        $listItems = new Collection();

        if (!empty($items)) {
            foreach($items as $key => $value){
                $itemValue = new Blog();
                $itemValue->id = $value['_source']['id'] ?? 0;
                $itemValue->alias = $value['_source']['alias'] ?? '';
                $itemValue->title = $value['_source']['title'] ?? '';
                $itemValue->description = $value['_source']['description'] ?? '';
                $itemValue->detail = $value['_source']['detail'] ?? '';
                $itemValue->meta_description = $value['_source']['meta_description'] ?? '';
                $itemValue->meta_keyword = $value['_source']['meta_keyword'] ?? '';
                $itemValue->images = $value['_source']['images'] ?? '';
                $listItems->add($itemValue);
            }
        }

        return $listItems;
    }


    /**
     * Get blog from elastic search and return to pagination
     *
     * @param $query
     * @param $page
     * @param $route
     * @return LengthAwarePaginator
     */
    public static function getPaginateByQuery($query, $page, $route)
    {
        $perPage = 15;

        $esClient = ClientBuilder::create()->build();
        try{
            $listPost = $esClient->search([
                'index' => env('ELASTICSEARCH_INDEX_NAME'),
                'from' => ($page - 1) * $perPage,
                'size' => $perPage,
                'q' => $query
            ]);
        }catch (\Exception $ex){

        }

        // difference between 2 version of elastic in local and server
        $totalItem = !empty($listPost['hits']['total']['value'])
            ? $listPost['hits']['total']['value']
            : (!empty($listPost['hits']['total']) ? (int)($listPost['hits']['total']) : 0);

        $totalPage = $totalItem % $perPage == 0 ? $totalItem / $perPage : ($totalItem / $perPage) + 1;

        $items = $listPost['hits']['hits'] ?? [];
        $listItems = [];
        if (!empty($items)) {
            foreach($items as $key => $value){
                $itemValue = new Blog();
                $itemValue->id = $value['_source']['id'] ?? 0;
                $itemValue->alias = $value['_source']['alias'] ?? '';
                $itemValue->title = $value['_source']['title'] ?? '';
                $itemValue->description = $value['_source']['description'] ?? '';
                $itemValue->detail = $value['_source']['detail'] ?? '';
                $itemValue->meta_description = $value['_source']['meta_description'] ?? '';
                $itemValue->meta_keyword = $value['_source']['meta_keyword'] ?? '';
                $itemValue->images = $value['_source']['images'] ?? '';
                $listItems[] = $itemValue;
            }
        }

        $return = new LengthAwarePaginator($listItems, $totalPage, $perPage, $page, [
            'path' => $route
        ]);

        return $return;
    }
}