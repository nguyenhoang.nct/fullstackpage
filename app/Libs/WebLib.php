<?php

namespace App\Libs;

use App\Models\BlogCategory;
use App\Models\BlogSearch;
use App\Models\Setting;
use Illuminate\Support\Facades\Cache;
class WebLib
{
    /**
     * Get setting by key
     *
     * @param $key
     * @return mixed
     */
    public static function getSetting($key)
    {
        return Cache::remember('setting::' . $key, now()->addMinute(60), function () use ($key) {
            $setting = Setting::where('setting_key', $key)->first();
            if (empty($setting)) {
                return false;
            }

            return $setting->setting_value;
        });
    }

    /**
     * Get Blog category
     *
     * @return mixed
     */
    public static function getBlogCategory()
    {
        return Cache::remember('blog_category', now()->addMinute(60), function () {
            return BlogCategory::where('parent_id', 0)->get();
        });
    }


    public static function getRandomSearch()
    {
        $randomSearch = BlogSearch::select('keyword', 'alias')->inRandomOrder()->limit(15)->get();

        return $randomSearch;
    }
}

?>