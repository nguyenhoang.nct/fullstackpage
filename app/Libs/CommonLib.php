<?php

namespace App\Libs;

use App;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;

class CommonLib
{

    /**
     * @param LengthAwarePaginator $listPost
     * @return null|string|string[]
     */
    public static function prettyPaginationUrl(LengthAwarePaginator $listPost)
    {
        $links = $listPost->links();
        $patterns = [];
        $patterns[] = '/'.$listPost->currentPage().'\?page=/';
        $replacements = array();
        $replacements[] = '';
        return preg_replace($patterns, $replacements, $links);
    }


    /**
     * Function set location to get language
     */
    public static function setLocale()
    {
        $lang = session('lang');
        if (empty($lang)) {
            $lang = 'vi';
        }
        App::setLocale($lang);
    }


    /**
     * Function get validation error
     * @param $validator
     * @return string
     */
    public static function getValidationError($validator)
    {
        $error = $validator->errors();
        $errorMessage = '';
        if (!empty($error)) {
            foreach ($error->all() as $key => $value) {
                $errorMessage .= $value . '</br>';
            }
        }
        $error = $errorMessage;
        return $error;
    }


    /**
     * substring a long content and add button show more
     * @param $string
     * @param int $length
     * @return string
     */
    public static function subStringAndShowMore($string, $length = 10)
    {
        $subString = Str::words(strip_tags($string), $length);
        if ($subString != strip_tags($string)) {
            $html = '<div class="description_tab">
                <div class="description_tab_content description_tab_show">' . $subString . '...<a class="show_description_tab" data-type="hide">Hiển thị thêm</a></div>
                <div class="description_tab_content description_tab_hide" style="display: none;">' . nl2br(strip_tags($string)) . ' <a class="show_description_tab" data-type="show">Hiển thị bớt</a><div>
            </div>';
        } else {
            $html = strip_tags($string);
        }
        return $html;
    }


    /**
     * convert date from old format to new format
     * @param $date
     * @param $toFormat
     * @return false|string
     */
    public static function convertDate($date, $toFormat)
    {
        $dateArray = explode('/', $date);
        $year = !empty($dateArray[2]) ? $dateArray[2] : 1970;
        $month = !empty($dateArray[1]) ? $dateArray[1] : 01;
        $date = !empty($dateArray[0]) ? $dateArray[0] : 01;
        return date($toFormat, strtotime($year.'-'.$month.'-'.$date));
    }


    function check_date_month ( $date, $month, $year ) {
        $nam_nhuan = false;
        $min_date = 1;
        if ( $year % 4 == 0 || ($year % 100 == 0 && $year % 400 == 0) ) {
            $nam_nhuan = true;
        }

        if ( in_array( $month, array( 1, 3, 5, 7, 8, 10, 12 ) ) ) {
            $max_date = 31;
        } else if ( in_array( $month, array( 4, 6, 9, 11 ) ) ) {
            $max_date = 30;
        } else if ( $month == 2 ) {
            if ( $nam_nhuan ) {
                $max_date = 29;
            } else {
                $max_date = 28;
            }
        }

        if ( $date < $min_date || $date > $max_date ) {
            return false;
        } else {
            return true;
        }
    }


    function check_current_date ( $date, $month, $year ) {
        if ( $year == date( 'Y' ) ) {
            if ( $month < date( 'm' ) ) {
                return true;
            } else if ( $month == date( 'm' ) ) {
                if ( $date > date( 'd' ) ) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else if ( $year > date( 'Y' ) ) {
            return false;
        } else {
            return true;
        }
    }


    function encodeReUrl ( $url ) {
        return urlencode( base64_encode( $url ) );
    }


    function decodeReUrl ( $url ) {
        return base64_decode( urldecode( $url ) );
    }


    function slugFormat ( $str, $separator = '-' ) {
        $str = preg_replace( "/(å|ä|à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str );
        $str = preg_replace( "/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str );
        $str = preg_replace( "/(ì|í|ị|ỉ|ĩ)/", 'i', $str );
        $str = preg_replace( "/(ö|ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str );
        $str = preg_replace( "/(ü|ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str );
        $str = preg_replace( "/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str );
        $str = preg_replace( "/(đ)/", 'd', $str );
        $str = preg_replace( "/(č|ç)/", 'c', $str );
        $str = preg_replace( "/(š,ş)/", 's', $str );
        $str = preg_replace( "/(ğ)/", 'g', $str );
        $str = preg_replace( "/(ž)/", 'z', $str );
        $str = preg_replace( "/(Ä|Å|À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str );
        $str = preg_replace( "/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str );
        $str = preg_replace( "/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str );
        $str = preg_replace( "/(Ö|Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str );
        $str = preg_replace( "/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str );
        $str = preg_replace( "/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str );
        $str = preg_replace( "/(Č|Ç)/", 'C', $str );
        $str = preg_replace( "/(Đ)/", 'D', $str );
        $str = preg_replace( "/(Š)/", 'S', $str );
        $str = preg_replace( "/(Ž)/", 'Z', $str );
        $str = str_replace( " ", "-", str_replace( "&*#39;", "", $str ) );
        $str = preg_replace( "#\.(?!\s*html|php)#i", '-', $str );
        $str = preg_replace( '/[^A-Za-z0-9\-._]/', '-', $str ); // Removes special chars.
        $str = preg_replace( '/-+/', '-', $str );
        $str = strtolower( $str );
        return $str;
    }


    function wordsTrim ( $string, $count = 50, $ellipsis = '....' ) {
        $words = explode( ' ', strip_tags( $string ) );
        if ( count( $words ) > $count ) {
            array_splice( $words, $count );
            $string = implode( ' ', $words ) . $ellipsis;
        }
        return $string;
    }


    public static function checkRole($admin, $role)
    {
        if(!empty($role)){
            foreach($role as $key => $value){
                if($admin->can($value)){
                    return true;
                }
            }
        }

        return false;
    }
}

?>