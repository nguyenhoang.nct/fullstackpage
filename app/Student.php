<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
}
