<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Libs\WebLib;
use App\Models\Setting;
use Illuminate\Support\Facades\Config;

class SeoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // set seo meta
        Config::set('seotools.meta.defaults.title', WebLib::getSetting(Setting::HOME_SEO_TITLE));
        Config::set('seotools.meta.defaults.description', WebLib::getSetting(Setting::HOME_SEO_DESCRIPTION));
        Config::set('seotools.meta.defaults.keywords', explode(',', WebLib::getSetting(Setting::HOME_SEO_KEYWORD)));

        // set seo opengraph
        Config::set('seotools.opengraph.defaults.title', WebLib::getSetting(Setting::HOME_SEO_TITLE));
        Config::set('seotools.opengraph.defaults.description', WebLib::getSetting(Setting::HOME_SEO_DESCRIPTION));
        Config::set('seotools.opengraph.defaults.site_name', WebLib::getSetting(Setting::SITE_NAME));
        Config::set('seotools.opengraph.defaults.images', [WebLib::getSetting(Setting::LOGO)]);

        // twitter
        Config::set('seotools.twitter.defaults.site', WebLib::getSetting(Setting::TWITTER_SITE));
        Config::set('seotools.twitter.defaults.creator', WebLib::getSetting(Setting::TWITTER_CREATOR));
    }
}
