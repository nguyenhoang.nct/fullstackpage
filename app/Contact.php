<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';

    protected $fillable = [
      'full_name', 'phone', 'status'
    ];

    const STATUS = [
      1 => 'Đã học thử',
      0 => 'Chưa học thử',
    ];
}
