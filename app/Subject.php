<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
    protected $table = 'subjects';
    CONST STATUS = [
        1 => 'ON',
        0 => 'OFF'
    ];
    public function course()
    {
        return $this->belongsToMany('App\Course');
    }

}
