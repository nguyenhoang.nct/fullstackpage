<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningFacility extends Model
{
    //
    protected $table = "learning_facilities";
    CONST STATUS = [
        1 => 'Enable',
        0 => 'Disable'
    ];
    public function classRoom() {
    	return $this->hasMany('App\Classroom', 'learning_facility_id', 'id');
    }
}
