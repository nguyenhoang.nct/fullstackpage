<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class BlogCategory extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'alias', 'parent_id', 'image', 'meta_description', 'meta_keyword', 'status'
    ];

    /**
     * Relationship with parent category
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function parent()
    {
        return $this->belongsTo(BlogCategory::class, 'parent_id', 'id');
    }

    /**
     * Relation ship with child category
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child()
    {
        return $this->hasMany(BlogCategory::class, 'parent_id', 'id');
    }

    /**
     * Filter by parent id
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeParentId($query, $filter)
    {
        return !empty($filter) ? $query->where('parent_id', $filter) : $query;
    }


    /**
     * Get list category
     *
     * @param Request $request
     * @return mixed
     */
    public static function getList(Request $request)
    {
        $limit = $request->limit ? $request->limit : self::$PER_PAGE;
        return self::name($request->name)
            ->parentId($request->parent_id)
            ->status($request->status)
            ->sortOrder($request)
            ->paginate($limit);
    }

    /**
     * Get all blog category
     *
     * @return Collection
     */
    public static function getAll()
    {
        return Cache::remember('BlogCategory::getAll', 120, function () {
            return BlogCategory::get();
        });
    }
}
