<?php

namespace App\Models;

use Illuminate\Http\Request;

class BlogSearch extends BaseModel
{
    protected $fillable = [
        'keyword', 'alias', 'alias'
    ];

    /**
     * Filter by keyword
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeKeyword($query, $filter)
    {
        return !empty($filter) ? $query->where('keyword', 'like', "%{$filter}%") : $query;
    }


    /**
     * Get list keyword
     *
     * @param Request $request
     * @return mixed
     */
    public static function getList(Request $request)
    {
        $limit = $request->limit ? $request->limit : self::$PER_PAGE;
        return self::keyword($request->keyword)
            ->sortOrder($request)
            ->paginate($limit);
    }
}
