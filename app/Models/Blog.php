<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class Blog extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'category_id', 'type', 'title', 'alias', 'images', 'description',
        'detail', 'meta_description', 'meta_keyword', 'source', 'published_at'
    ];

    /**
     * Relationship with category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'category_id', 'id');
    }


    /**
     * Relation ship with tag
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tags()
    {
        return $this->hasMany(BlogTag::class);
    }

    /**
     * Filter by post title
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeTitle($query, $filter)
    {
        return !empty($filter) ? $query->where('title', 'like', "%{$filter}%") : $query;
    }

    /**
     * Filter by post category ID
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeCategoryId($query, $filter)
    {
        return !empty($filter) ? $query->where('category_id', $filter) : $query;
    }

    /**
     * Get list post
     *
     * @param Request $request
     * @return mixed
     */
    public static function getList(Request $request)
    {
        $limit = $request->limit ? $request->limit : self::$PER_PAGE;
        return self::title($request->title)
            ->categoryId($request->category_id)
            ->sortOrder($request)
            ->paginate($limit);
    }


    /**
     * Get 5 newest post and show them to home page
     * These posts not show in category box below
     *
     * @return Collection
     */
    public static function getHomeNewestPost()
    {
        return Cache::remember('Blog::getHomeNewestPost', 3600, function () {
            return Blog::orderBy('id', 'DESC')->whereNotNull('images')->where('images', '<>', '')->limit(5)->get();
        });
    }


    /**
     * Get posts by category at home page
     * Ignore posts that exist in home newest post
     *
     * @param $listCategory
     * @param $listNewestPostId
     * @return array
     */
    public static function getHomePostByCategory($listCategory, $listNewestPostId)
    {
        return Cache::remember('Blog::getHomePostByCategory', 3600, function () use ($listCategory, $listNewestPostId) {
            $postByCategory = [];
            foreach ($listCategory as $key => $value) {
                $postByCategory[$value->id] = Blog::orderBy('id', 'DESC')
                    ->where('category_id', $value->id)
                    ->whereNotIn('id', $listNewestPostId)
                    ->whereNotNull('images')->where('images', '<>', '')
                    ->limit(5)->get();
            }
            return $postByCategory;
        });
    }
}
