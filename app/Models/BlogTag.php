<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class BlogTag extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'alias', 'blog_id'
    ];

    /**
     * Relation ship to blog
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blog()
    {
        return $this->belongsTo(Blog::class, 'blog_id', 'id');
    }

    /**
     * Filter by Blog ID
     *
     * @param $query
     * @param $filter
     * @return mixed
     */
    public function scopeBlogId($query, $filter)
    {
        return !empty($filter) ? $query->where('blog_id', $filter) : $query;
    }

    /**
     * Get list blog tag
     *
     * @param Request $request
     * @return mixed
     */
    public static function getList(Request $request)
    {
        $limit = $request->limit ? $request->limit : self::$PER_PAGE;
        return self::name($request->name)
            ->blogId($request->blog_id)
            ->sortOrder($request)
            ->paginate($limit);
    }
}
