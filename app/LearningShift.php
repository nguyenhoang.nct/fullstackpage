<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningShift extends Model
{
    public function classes()
    {
        return $this->hasMany('App\Classes', 'learning_shift_id', 'id');
    }
}
