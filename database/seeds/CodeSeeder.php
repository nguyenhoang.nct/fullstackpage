<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 1; $i <= 2000; $i++)
        {
            DB::table('codes')->insert([
                'code' => strtoupper('fsa'.str_random('5')),
                'status' => 1
            ]);
        }
    }
}
