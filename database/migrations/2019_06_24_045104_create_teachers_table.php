<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 150)->nullable();
            $table->text('avatar')->nullable();
            $table->string('job', 150)->nullable();
            $table->text('fb')->nullable();
            $table->text('twitter')->nullable();
            $table->text('note')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->index(['full_name']);
            $table->index(['job']);
            $table->index(['status']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teachers');
    }
}
