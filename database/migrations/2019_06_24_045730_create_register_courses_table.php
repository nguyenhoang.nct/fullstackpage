<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegisterCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 150)->nullable();
            $table->string('phone', 150)->nullable();
            $table->string('email', 150)->nullable();
            $table->string('course_name', 150)->nullable();
            $table->index(['full_name']);
            $table->index(['phone']);
            $table->index(['email']);
            $table->index(['course_name']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_courses');
    }
}
