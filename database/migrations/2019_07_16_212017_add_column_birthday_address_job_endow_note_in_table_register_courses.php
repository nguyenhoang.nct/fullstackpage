<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBirthdayAddressJobEndowNoteInTableRegisterCourses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('register_courses', function (Blueprint $table) {
            $table->date('birthday')->nullable();
            $table->text('address')->nullable();
            $table->string('job', 150)->nullable();
            $table->integer('endow')->nullable();
            $table->text('note')->nullable();
            $table->index(['job']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('register_courses', function (Blueprint $table) {
            //
        });
    }
}
