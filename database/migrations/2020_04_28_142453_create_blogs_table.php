<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->nullable()->comment('Category ID');
            $table->string('title', 500)->nullable()->comment('Title');
            $table->string('alias', 500)->nullable()->comment('Alias');
            $table->string('type', 500)->nullable()->comment('Title');
            $table->string('images', 255)->nullable()->comment('Image');
            $table->text('description')->nullable()->comment('Description');
            $table->longText('detail')->nullable()->comment('Detail');
            $table->string('meta_description', 500)->nullable()->comment('Meta description');
            $table->string('meta_keyword', 500)->nullable()->comment('Meta keyword');
            $table->string('source', 255)->nullable()->comment('Source');
            $table->integer('status')->nullable()->comment('Status of post');
            $table->timestamp('published_at')->nullable()->comment('Published date');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
