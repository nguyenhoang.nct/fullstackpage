<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->integer('classes_id')->nullable();
            $table->integer('type')->nullable();
            $table->string('phone')->nullable();
            $table->text('address')->nullable();
            $table->text('message')->nullable();
            $table->string('status')->nullable();
            $table->text('note')->nullable();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

    }
}
