<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->nullable()->comment('Type alias');
            $table->string('alias', 255)->nullable()->comment('Type text');
            $table->integer('parent_id')->nullable()->comment('Parent ID');
            $table->string('image', 200)->nullable()->comment('Image');
            $table->string('meta_description', 500)->nullable()->comment('Meta description');
            $table->string('meta_keyword', 500)->nullable()->comment('Meta keyword');
            $table->integer('status')->nullable()->comment('Status of category');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_categories');
    }
}
