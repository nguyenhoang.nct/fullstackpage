<section class="benefits section-spacing" id="">
    <div class="container">
                <header class="section-header">
                    <h2 class="text-center">Ai Có Thể Học Tại Fullstack Academy ?</h2>
{{--                    <h3>Reasons why you should choose this product</h3>--}}
                </header>
                <div class="row">
                    <div class="col-sm-4"> <div class="text-center">
                        <img src="assets/img/benefits-1.png" alt="benefits of product"></div>
                        <h4 class="text-center">Lập Trình Viên</h4>
                            <span class="justify-text">
                                Khóa học của chúng tôi phù hợp với các lập trình viên muốn 
                                <i>học thêm ngôn ngữ</i> mới để đáp
                                ứng công việc, <i>tăng thêm thu nhập</i> cũng như mở rộng cơ hội 
                                <i>thăng tiến trong sự nghiệp</i>. Đừng ngần ngại, hãy liên hệ để chúng tôi giúp bạn.
                            </span>
                    </div>
                    <div class="col-sm-4"> <div class="text-center">
                        <img src="assets/img/benefits-2.png" alt="benefits of product"></div>
                        <h4 class="text-center">Học Sinh / Sinh Viên</h4>
                        <span class="justify-text">Với các bạn sinh viên muốn trải nghiệm 
                            <i>môi trường làm việc</i> 
                        ngay tại quá trình học tập. FullStack Academy với đội ngũ <i>giảng viên doanh nghiệp</i> 
                        nhiều kinh nghiệm và quy trình phát triển phần mềm tiên tiến nhất sẽ giúp bạn có được 
                        <i>kinh nghiệm thực tế</i> ngay khi đang học.</span>
                    </div>
                    <div class="col-sm-4"> <div class="text-center"><img src="assets/img/benefits-3.png" alt="benefits of product"></div>
                        <h4 class="text-center">Bất Cứ Ai</h4>
                        <span class="justify-text">Bạn chỉ cần yêu thích và đam mê lập trình hoặc muốn 
                            <i>chuyển đổi nghề nghiệp</i> 
                        để có cơ hội việc làm tốt hơn, FullStack Academy sẽ giúp bạn 
                        <i>định hướng và học lập trình</i> một cách tốt nhất, 
                        thỏa mãn <i>niềm đam mê</i> của bạn.</span>
                    </div>
                    
                </div>
            </div>
</section>
<section class="benefits section-spacing" id="">
    <div class="container">
        <header class="section-header">
            <h2 class="text-center">Vì Sao Nên Chọn Fullstack Academy ?</h2>
        </header>
        <div class="row">
            <div class="col-sm-4"> <div class="text-center"><img src="assets/img/benefits-1.png" alt="benefits of product"></div>
                <h4 class="text-center">Hệ Thống Mentor 24/7</h4>
                <span class="justify-text">
                    Hệ thống mentor của chúng tôi giúp bạn <i>định hướng nghề nghiệp</i>, hỗ trợ 
                    <i>xử lý vấn đề học tập 24/7</i> thông qua các nhóm và hệ thống chat, giúp bạn 
                    tiếp thu kiến thức <i>nhanh và hiệu quả</i> nhất. 
                </span>
            </div>
            <div class="col-sm-4"> <div class="text-center"><img src="assets/img/benefits-2.png" alt="benefits of product"></div>
                <h4 class="text-center">Đào Tạo Thực Tiễn</h4>
                <span class="justify-text">
                    Chương trình học tối ưu để học viên có thể <i>học kĩ năng thực tế</i> 
                    thông qua các <i>dự án thật</i> cùng với <i>giảng viên doanh nghiệp</i> 
                    đến từ các công ty hàng đầu Việt Nam.
                </span>
            </div>
            <div class="col-sm-4"> <div class="text-center"><img src="assets/img/benefits-3.png" alt="benefits of product"></div>
                <h4 class="text-center">Đảm Bảo Đầu Ra</h4>
                <span class="justify-text">
                    Cơ hội <i>thực tập và làm chính thức</i> tại các công ty lớn chuyên về 
                    <i>sản phẩm và gia công phần mềm</i> ngay sau khi kết thúc khóa học.
                    Ngoài ra, các bạn còn có cơ hội thực tập ở các <i>công ty thành viên</i> 
                    của FullStack Group.
                </span>
            </div>
        </div>
    </div>
</section>
