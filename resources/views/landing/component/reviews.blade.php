<section class="reviews section-spacing" id="reviews">
            <div class="container">
                <header class="section-header text-center">
                    <h2>Tâm Sự Từ Học Viên</h2>
{{--                    <h3>What our users are saying about the product</h3>--}}
                </header>
                <div class="row">
                    @foreach($students as $student)
                    <div class="col-sm-4">
                        <!--review 1-->
                        <figure class="text-center"><img src="/storage/{{ $student->avatar }}" alt="face" class="img-circle"> </figure>
                        <blockquote>
                            <p>{!! $student->note !!}</p>
                            <cite>— {{ $student->full_name }}, {{ $student->job }}</cite>
                        </blockquote>
                        <!--review 1 end-->
                    </div>
                    @endforeach
                </div>
            </div>
        </section>
