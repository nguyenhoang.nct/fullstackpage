<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="{{ asset('/') }}"><img class="logo-nav" alt="logo" src="/assets/img/logo-fa.png"><img class="logo-head" alt="logo" src="/assets/img/logo-no-bg.png"></a> </div>
        <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
            <ul class="nav navbar-nav nav-left">
                <li><a href="/#educate">Đào Tạo</a></li>
                <li><a href="/#course">Các Khóa Học</a></li>
                <li><a href="/#features">Dịch Vụ</a></li>
                <li><a href="/#team">Đội Ngũ</a></li>
                <li><a href="/#contact">Liên Hệ</a></li>
                {{-- <li><a href="{{ route('blog.index') }}">Blogs</a></li> --}}
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="cta-btn">
                        <a target="_blank" href="https://m.me/academy.fullstackcorp" class="btn subscribe-button">
                            ĐĂNG KÝ NGAY
                        </a>
                    </div>
                </li>
                <!-- <li><a href="{{ route('register_free') }}" class="btn">ĐĂNG KÝ KHOÁ HỌC MIỄN PHÍ NGAY</a></li> -->
            </ul>
        </div>
    </div>
</nav>
