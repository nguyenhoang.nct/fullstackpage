<!--site-footer-->
<footer class="site-footer section-spacing">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 margin_xs">
                <a href="/"><img class="logo-nav" alt="logo" src="/assets/img/logo-no-bg.png"></a>
            </div>
            <div class="col-md-4 col-sm-6 margin_xs">
                <h3 style="text-align: left;">LIÊN HỆ</h3>
                <ul class="contacting">
                    <li>
                        <i class="fa fa-home" style="margin-right: 10px;"></i>
                        <a href="javascript:void(0)">Trụ sở: Tầng 4, Tháp 2, Times Tower, 35 Lê Văn Lương, Thanh Xuân,
                            Hà Nội</a>
                    </li>
                    <li>
                        <i class="fa fa-home" style="margin-right: 10px;"></i>
                        <a href="javascript:void(0)">Cơ sở 2: 214 Nguyễn Lương Bằng, Đống Đa, Hà Nội</a>
                    </li>
                    <li>
                        <i class="fa fa-phone" style="margin-right: 10px;"></i>
                        <a href="tel:0985333198">Hotline: 0985.333.198</a>
                    </li>
                    <li>
                        <i class="fa fa-envelope" style="margin-right: 10px;"></i>
                        <a href="mailto:longnh@baobinh.net">Email: longnh@baobinh.net</a>
                    </li>
                    <li><i class="fa fa-users" style="margin-right: 10px;"></i><a target="_blank"
                            href="https://www.facebook.com/groups/giaodienweb/">Tham gia group</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 margin_xs">
                <div class="fb-page" data-href="https://www.facebook.com/academy.fullstackcorp/" data-tabs=""
                    data-width="" data-height="" data-small-header="false" data-adapt-container-width="true"
                    data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/academy.fullstackcorp/" class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/academy.fullstackcorp/">FullStack Academy - Học qua dự án
                            thực tế với giảng viên đến từ doanh nghiệp</a></blockquote>
                </div>
            </div>
        </div>
        <!--chat-btn-->
        <a href="#" class="chat-btn" data-toggle="modal" data-target="#modal-contact-form"></a>
        <!--chat-btn end-->
    </div>
</footer>
<!--site-footer end-->
