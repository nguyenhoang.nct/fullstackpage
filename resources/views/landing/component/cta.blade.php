<section class="cta-section section-spacing text-center">
            <div class="container">
                <header class="section-header">
                    <h2>Hãy Đăng Ký Ngay Hôm Nay!</h2>
                    <h3>Giảm trực tiếp tối đa <b>5.000.000</b> VNĐ học lập trình tại FullStack Academy khi đăng ký trước {{ date("t/m") }}.</h3>
                </header>
                <div class="row">
                    <div class="col-md-12"> <a target="_blank" href="https://m.me/academy.fullstackcorp" class="btn subscribe-button">ĐĂNG KÝ</a>
                        <p>Bạn vẫn còn phân vân? Hãy gọi cho chúng tôi
                            <a href="tel:0985333198"> 0985.333.198</a>
                            để được tư vấn.
                        </p>
                    </div>
                </div>
            </div>
        </section>
