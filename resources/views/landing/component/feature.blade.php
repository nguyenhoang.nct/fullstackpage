<div class="features section-spacing" id="features">
            <div class="container">
                <!--feature 1-->
                <div class="row">
                    <div class="col-md-7 col-md-push-5 text-center" style="height: 518px;"> <img src="assets/img/frontend-01.png" alt="feature"> </div>
                    <div class="col-md-5 col-md-pull-7">
                        <article>
                            <h2>Khóa học Front End</h2>
                            <span>Kết thúc khóa học, học viên sẽ đạt được:</span>
                            <ul>
                                <li>Có định hướng, tư duy và thái độ làm việc tốt</li>
                                <li>Các công cụ làm việc thực tế như Slack, Trello, Git, Gitlab, Zeplin, Photoshop,...</li>
                                <li>Cắt giao diện từ file PSD, Sketch, XD, Zeplin,...</li>
                                <li>Biết cách sử dụng Bootstrap để cắt giao diện responsive</li>
                                <li>Biết cách sử dụng Javascript và Jquery</li>
                                <li>Nắm vững được cách sử dụng framework React.Js để xây dựng web app</li>
                                <li>Có 3 tháng kinh nghiệm thực tập tại FullStack Technology</li>
                                <li style="color: #f56363">Tương đương với Fresher frontend developer</li>
                            </ul>
                        </article>
                    </div>
                </div>
                <!--feature 1 end-->
                <!--feature 2-->
                <div class="row">
                    <div class="col-md-7 text-center" style="height: 518px;"> <img src="assets/img/backend-01.png" alt="feature"> </div>
                    <div class="col-md-5">
                        <article>
                            <h2>Khóa học Back End</h2>
                            <span>Kết thúc khóa học, học viên sẽ đạt được:</span>
                            <ul>
                                <li>Có định hướng, tư duy và thái độ làm việc tốt</li>
                                <li>Các công cụ làm việc thực tế như Slack, Trello, Git, Gitlab, Zeplin, Photoshop,...</li>
                                <li>Nắm chắc kiến thức về cơ sở dữ liệu MySQL</li>
                                <li>Nắm vững các kiến thức lập trình Nodejs và framework Express.Js</li>
                                <li>Nắm vững PHP cơ bản và framework Laravel để xây dựng các dự án thực tế</li>
                                <li>Có hiểu biết về lập trình OOP, MVC và Restful API</li>
                                <li>Có 3 tháng kinh nghiệm thực tập tại FullStack Technology</li>
                                <li style="color: #f56363">Tương đương với Fresher backend developer</li>
                            </ul>
                        </article>
                    </div>
                </div>
                <!--feature 2 end-->
                <!--feature 3-->
                <div class="row">
                    <div class="col-md-7 col-md-push-5 text-center" style="height: 518px;"> <img src="assets/img/fullstack-01.png" alt="feature"> </div>
                    <div class="col-md-5 col-md-pull-7">
                        <article>
                            <h2>Khóa học FullStack</h2>
                            <span>Kết thúc khóa học, học viên sẽ đạt được:</span>
                            <ul>
                                <li>Có định hướng, tư duy và thái độ làm việc tốt</li>
                                <li>Sử dụng tốt các công cụ làm việc thực tế như Slack, Trello, Gitlab, Zeplin, Photoshop,...</li>                                
                                <li>Cắt giao diện từ file PSD, Sketch, XD, Zeplin,...</li>
                                <li>Biết cách sử dụng Bootstrap để cắt giao diện responsive</li>
                                <li>Biết cách sử dụng Javascript và Jquery</li>
                                <li>Nắm vững được cách sử dụng framework React.Js để xây dựng web app</li>
                                <li>Nắm chắc kiến thức về cơ sở dữ liệu MySQL</li>
                                <li>Nắm vững các kiến thức lập trình Nodejs và framework Express.Js</li>
                                <li>Nắm vững PHP cơ bản và framework Laravel để xây dựng các dự án thực tế</li>
                                <li>Có hiểu biết về lập trình OOP, MVC và Restful API</li>
                                <li>Có 3 tháng kinh nghiệm thực tập tại FullStack Technology</li>
                                <li style="color: #f56363">Tương đương với FullStack developer 1 năm kinh nghiệm</li>
                            </ul>
                        </article>
                    </div>
                </div>
                <!--feature 3 end-->
            </div>
        </div>