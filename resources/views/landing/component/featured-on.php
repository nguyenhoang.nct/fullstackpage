<section id="educate" class="featured-on section-spacing text-center">
  <div class="container">
    <header class="section-header">
      <h2>Chúng Tôi Đào Tạo</h2>
    </header>
    <div class="row">
      <div class="col-md-12">
        <ul class="featured-sites">
          <li><a href="javascript:void(0)" title="CSS, HTML, Javascript"><img src="assets/img/css-html-js.png" alt="CSS, HTML, Javascript" width="100"></a></li>
          <li><a href="javascript:void(0)" title="PHP"><img src="assets/img/php.png" alt="CSS, HTML, Javascript" width="100" ></a> </li>
          <li><a href="javascript:void(0)" title="Node.js"><img src="assets/img/nodejs.png" alt="CSS, HTML, Javascript" width="100" ></a></li>
          <li><a href="javascript:void(0)" title="React.js"><img src="assets/img/reactjs.png" alt="CSS, HTML, Javascript" width="100" ></a></li>
          <li><a href="javascript:void(0)" title="MySQL"><img src="assets/img/mysql.png" alt="MySQL" width="100" ></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
