    <div id="register" class="modal fade" role="dialog">

        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal">×</button>

                    <h2 class="modal-title">Đăng ký<ion-icon ios="ios-arrow-forward" md="md-arrow-forward" role="img" class="hydrated" aria-label=""></ion-icon>

                    </h2>

                </div>

                <div class="modal-body">

                    <div class="content-form">

                        <form id="register-form" method="post" action="">

                            <div class="form_group">

                                <label>Họ và tên</label>

                                <input class="form-control" type="text" name="register_name" id="register-name" placeholder="Nhập họ và tên...." >

                            </div>

                            <div class="form_group">

                                <label>Địa chỉ email</label>

                                <input class="form-control" type="email" name="register_email" id="register-email" placeholder="Nhập email..." >

                            </div>

                            <div class="form_group">

                                <label>Số điện thoại</label>

                                <input class="form-control" type="number" name="register_phone" id="register-phone" placeholder="Nhập SĐT..." >

                            </div>



                            <div class="form_group">

                                <label>Ngày sinh</label>

                                <input class="form-control" type="date" name="register_birthday" id="register-birthday">

                            </div>



                            <div class="form_group">

                                <label>Địa chỉ</label>

                                <input class="form-control" type="text" name="register_address" id="register-address" placeholder="Nhập địa chỉ...." >

                            </div>



                            <div class="form_group">

                                <label>Nghề nghiệp</label>

                                <input class="form-control" type="text" name="register_job" id="register-job" placeholder="Nghề nghiệp của bạn..." >

                            </div>



                            <div class="form_group">

                                <label>Ghi chú</label>

                                <textarea class="form-control" rows="5" name="register_note" id="register-note" placeholder="Ghi chú..." ></textarea>

                            </div>

                            

                            <div class="frm_group">

                                <label>Khóa học bạn chọn</label>

                                <select class="form-control form-select selcls" name="register_course" id="register-course">

                                    <option value="">---Khóa học---</option>

                                    @foreach($courses as $course)

                                        <option value="{{ isset($course) ? $course->id : '' }}">{{ isset($course) ? $course->title : '' }}</option>

                                    @endforeach

                                </select>

                            </div>

                            <div class="frm_group">

                                <button type="submit" id="btn-register" class="submit_register">Đăng ký</button>

                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div id="detail" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">

        <div class="modal-dialog">

            <!-- Modal content-->

            <div class="modal-content">

                <div class="modal-header">

                    <button type="button" class="close close-detail" data-dismiss="modal">×</button>

                    <h4 class="modal-title detail-title">

                    </h4>

                </div>

                <div class="modal-body">

                    <div class="content-detail">

                        <table class="table">

                            <thead>

                                <tr>

                                    <th colspan="2" class="th-detail"></th>

                                </tr>

                            </thead>

                            <tbody>

                            <tr>

                                <td>

                                    <div class="dot-pulse loading-detail center-block align-items-center h-100">

                                    </div>

                                 </td>
                                 
                            </tr>

                            </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    <style type="text/css" media="screen">
        .list__course {
            display: grid;
            grid-column: 1fr 1fr;
            grid-template-columns: repeat(2,1fr);
            grid-gap: 20px;
            padding-left: 2rem;
        }
        
    </style>