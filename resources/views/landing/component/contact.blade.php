<section class="contact section-spacing text-center" id="contact">
            <div class="container">
                <div class="row">
                	<div class="col-md-6 hide-xs">
                		<img src="assets/img/contactus-01.jpg" alt="feature">
                	</div>
                    <div class="col-md-6">
                        <div class="contact-form text-center">
                            <header class="section-header"> <img src="assets/img/support-icon.svg" alt="support icon">
                                <h2>Liên Hệ Với Chúng Tôi</h2>
                                <h3>Để được giải đáp những thắc mắc của bạn.</h3>
                            </header>
                            <form class="cta-form" id="contact-form" method="post">
                                <div class="form-group">
                                    <input type="text" name="name"  class="contact-name form-control input-lg" placeholder="Họ và tên *" id="contact-name">
                                </div>
                                <div class="form-group">
                                    <input type="number" name="phone"  class="contact-phone form-control input-lg" placeholder="SĐT *" id="contact-phone">
                                </div>
                                <input type="hidden" name="status" id="contact-status">
                                <button type="submit" id="btn-contact" class="btn">GỬI YÊU CẦU</button>
                            </form>
                        </div>
                        <div class="dot-pulse loading center-block align-items-center h-100"></div>
                        <span class="contact-form-success noti_contact"><i class="fa fa-check"></i><span>Cảm ơn bạn !</span><br> Chúng tôi sẽ liên hệ lại với bạn sớm nhất !</span>
                    </div>
                </div>
            </div>
        </section>
