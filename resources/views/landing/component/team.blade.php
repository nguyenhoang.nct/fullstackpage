<section class="team section-spacing text-center" id="team">
            <div class="container">
                <header class="section-header">
                    <h2>Đội Ngũ Giảng Dạy</h2>
                </header>
                <div class="row">
                    @foreach($teachers as $teacher)
                     <div class="col-sm-4">
                        <div class="team-details">
                            <figure><img src="/storage/{{ $teacher->avatar }}" alt="Team Member">
                                <figcaption>
                                    <div>
                                        <p>{!! $teacher->note !!}</p>
                                        <!--social-->
                                        <ul class="social">
                                            <li><a href="{{ $teacher->twitter }}"><i class="fa fa-envelope"></i></a></li>
                                            <li><a href="{{ $teacher->fb }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        </ul>
                                        <!--social end-->
                                    </div>
                                </figcaption>
                            </figure>
                            <div class="team-info">
                                <h4>{{ $teacher->full_name }}</h4>
                                <h5>{{ $teacher->job }}</h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
</section>
