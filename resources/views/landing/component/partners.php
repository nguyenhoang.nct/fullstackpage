<section class="featured-on partners section-spacing text-center">
  <div class="container">
    <header class="section-header">
      <h2>Đối Tác Tuyển Dụng</h2>
    </header>
    <div class="row">
      <div class="col-md-12">
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="FullStack Technology">
                    <img src="assets/img/logo-fullstack.png" alt="FullStack Technology" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="Space Share">
                        <img src="assets/img/space-share-logo.png" alt="Space Share" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="Tập đoàn công nghệ G-Group">
                    <img src="assets/img/ggroup.png" alt="Tập đoàn công nghệ G-Group" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="MOR Software">
                    <img src="assets/img/mor-software.png" alt="MOR Software" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="Cheese.vn">
                    <img src="assets/img/cheese.png" alt="Cheese.vn" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="BaoBinh.Net">
                    <img src="assets/img/baobinh.net.png" alt="BaoBinh.Net" width="100">
                </a>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="Baobinh.net">
                    <img src="assets/img/css-html-js.png" alt="Baobinh.net" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="Cheese.vn">
                    <img src="assets/img/css-html-js.png" alt="Cheese.vn" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="FullStack Technology">
                    <img src="assets/img/css-html-js.png" alt="FullStack Technology" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="FullStack Technology">
                    <img src="assets/img/css-html-js.png" alt="FullStack Technology" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="FullStack Technology">
                    <img src="assets/img/css-html-js.png" alt="FullStack Technology" width="100">
                </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2 item-group">
                <a href="javascript:void(0)" title="FullStack Technology">
                    <img src="assets/img/css-html-js.png" alt="FullStack Technology" width="100">
                </a>
            </div>
        </div> -->
      </div>
    </div>
  </div>
</section>
