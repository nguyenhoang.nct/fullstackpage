<section class="section-spacing" id="course">
    <div class="container">
        <header class="section-header text-center">
            <h2>Khóa Học Lập Trình</h2>
        </header>
        <header class="heading">
            <span class="caro-prev other-prev"><i class="fa fa-angle-double-left"></i></span>
            <span class="caro-next other-next"><i class="fa fa-angle-double-right"></i></span>
        </header>
        <div class="carousel-multiple owl-carousel owl-theme">
            @foreach($courses as $course)
            <div class="item">
                <div class="thumbnail-product thumbnail">
                    <figure class="image-zoom">
                        <img width="360" height="270" src=""
                    class="attachment-course_thumbnail_home size-course_thumbnail_home wp-post-image" alt="{{ $course->title }}"
                            srcset="/storage/{{ $course->feature_image }} 360w, /storage/{{ $course->feature_image }} 510w"
                            sizes="(max-width: 360px) 100vw, 360px">
                    </figure>
                    <div class="caption">
                        <div class="cate">
                            <h6>KHÓA HỌC</h6>
                        </div>
                        <h3>
                            <a href="{{ route('course', ['id' => $course->id, 'alias' => $course->alias]) }}"
                                class="detail-course">
                                {{ $course->title }}
                            </a>
                        </h3>

                        {{-- <div class="single-item-content">
                            <p class="prices">
                                @if($course->promotion_price != null)
                                <del>{{ number_format($course->price, 0, ',','.') }}</del>
                                <ins>{{ number_format($course->promotion_price, 0, ',','.') }}</ins>
                                @else
                                <ins>{{ number_format($course->price, 0, ',','.') }}</ins>
                                @endif
                            </p>
                        </div> --}}

                        <div class="button-bottom">
                            <a href="{{ route('course', ['id' => $course->id, 'alias' => $course->alias]) }}"
                                class="button-detail detail-course">Chi tiết</a>
                            <a target="_blank" class="button-default subscribe-button"
                                href="https://m.me/academy.fullstackcorp">ĐĂNG KÝ NGAY</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
