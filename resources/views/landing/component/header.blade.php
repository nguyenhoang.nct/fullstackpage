        <!--hero section-->
        <header class="hero-section">
            <!--navigation-->
            @include('landing.component.navbar')
            <!--navigation end-->
            <!--welcome message-->
            <section class="container text-center welcome-message">
                <div class="row">
                    <div class="col-md-12">
                        <h2>FULLSTACK ACADEMY</h2><br/>
                        <h3>Học lập trình qua dự án thực tế cùng giảng viên doanh nghiệp</h3>
                        <div class="play-btn">
                        </div>
                        <div class="cta-btn">
                            <a target="_blank" href="https://www.m.me/academy.fullstackcorp" class="btn subscribe-button">ĐĂNG KÝ NGAY</a>
                            <p>
                                Giảm tối đa
                                <span class="discount">5.000.000 VNĐ</span>
                                khi đăng ký trước {{ date("t/m") }}
                            </p>
                        </div>
                    </div>
                </div>
            </section>
            <!--welcome message end-->
        </header>
        <!--hero section end-->
