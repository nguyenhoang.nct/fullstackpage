<!DOCTYPE html>
<html lang="vi">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,300,700" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-language" content="vi" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('pageTitle')</title>
    <meta name="twitter:site" content="http://academy.fullstackcorp.com/" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:image" content="/assets/img/frontend-01.png" />
    <meta property="og:url" content="http://academy.fullstackcorp.com/" />
    <meta property="og:title" content="@yield('pageTitle')" />
    <meta property="og:description" content="FullStack Academy - Học lập trình qua dự án với giảng viên doanh nghiệp"/>
    <meta property="og:image" content="/assets/img/frontend-01.png" />
    <meta name="description" content="FullStack Academy - Học lập trình qua dự án với giảng viên doanh nghiệp">
    <meta name="robots" content="noodp,index,follow" />
    <meta name="revisit-after" content="1 days" />
    <link rel="shortcut icon" href="/assets/img/logo-fa.png" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="/assets/img/logo-fa.png" type="image/x-icon">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '326663064968181');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=326663064968181&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->


    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/three-dots.min.css">
    <!-- owl.carousel CSS
    ============================================ -->
    <link rel="stylesheet" href="/assets/css/owl.carousel.css">
    <!-- owl.theme CSS
    ============================================ -->
    <link rel="stylesheet" href="/assets/css/owl.theme.css">
    <!-- owl.transitions CSS
    ============================================ -->
    <link rel="stylesheet" href="/assets/css/owl.transitions.css">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="/assets/css/magnific-popup.css" rel="stylesheet">
    <link href="/assets/css/main.css" rel="stylesheet">
</head>
<body>
<div class="wrapper">
    @yield('content')
</div>
@include('landing.component.footer')
    <div class="back-to-top">
        <i class="fa fa-arrow-up"></i>
    </div>
    <a href="tel:0985333198">
            <div class="hotline">
                    <div class="icon-local"></div>
                    <div class="icon"></div>
                    <div class="fa fa-phone local"></div>
            </div>
    </a>
    <script src="/assets/js/jquery-2.1.4.min.js"></script>
    <!-- Go to www.addthis.com/dashboard to customize your tools -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5d256c5d8233a707"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <!-- owl.carousel.min js -->
    <script src="/assets/js/owl.carousel.min.js"></script>
    <script src="/assets/js/waypoints.min.js"></script>
    <script src="/assets/js/jquery.animateNumber.min.js"></script>
    <script src="/assets/js/waypoints-sticky.min.js"></script>
    <script src="/assets/js/jquery.magnific-popup.min.js"></script>
    <script src="/assets/js/tweetie.min.js"></script>
    <script src="/assets/js/main.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{ asset('/assets/js/jquery.validate.min.js') }}"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSUq0I2FCVkhgjNR6fTRbqm19ULreKA4k&amp;callback=initMap"></script>  -->
    <script src="https://unpkg.com/ionicons@4.2.0/dist/ionicons.js"></script>
    <script src="/assets/js/ajax.js"></script>
    @yield('script')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165845782-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-165845782-1');
    </script>
    <!-- end Google analytics -->

    <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="105113211160925"
		logged_in_greeting="Xin chào, hãy để FullStack Academy tư vấn cho bạn!"
		logged_out_greeting="Xin chào, hãy để FullStack Academy tư vấn cho bạn!">
      </div>

    <style>
        .fb_dialog {
          position: -webkit-sticky !important;
          position: fixed !important;
          bottom: 95px !important;
          right: 15px !important;
        }
    </style>
    <script type="text/javascript">
        var btnRegister = document.getElementsByClassName('subscribe-button');
        for (var i = 0 ; i < btnRegister.length; i++) {
            btnRegister[i].addEventListener('click', function() {
                fbq('track', 'Contact');
            }, false);
        }
    </script>
</body>
</html>
