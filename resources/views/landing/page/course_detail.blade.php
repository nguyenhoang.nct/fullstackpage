@extends('landing.layout.master')

@section('pageTitle')
{{ $course->title }}
@endsection

@section('content')
@include('landing.component.header-only')
<div class="container content-detail course-detail">
    <div class="row">
        <div class="col-xs-12 cold-sm-12 col-md-9 col-lg-9">
            <article>
                <h1>Khóa Học: {{ $course->title }}</h1>
                <div class="row description">
                    <div class="col-xs-12 text-center">
                        <h2>Ưu điểm của khóa học {{ $course->title }}</h2>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                        {!! $course->description !!}
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="row course-summary">
                            <table class="table table-tripped" style="border: none !important;">
                                <tr>
                                    <th>Học phí:</th>
                                    <td>
                                        <p class="course-price">
                                            <a target="_blank" class="btn btn-sm" href="https://www.m.me/academy.fullstackcorp" class="btn subscribe-button">Hỏi giá</a>

                                            {{-- <span>{{ number_format($course->price, 0, ",", ".") }} VNĐ</span> --}}
                                        </p>
                                        {{-- <p class="course-discount-price">
                                            Khuyến mại:
                                            <span>{{ number_format($course->promotion_price, 0, ",",".") }} VNĐ</span>
                                        </p> --}}
                                    </td>
                                </tr>
                                <tr>
                                    <th>Số học kỳ:</th>
                                    <td>{{ $course->number_sessions }}</td>
                                </tr>
                                <tr>
                                    <th>Số buổi học:</th>
                                    <td>{{ $course->total_day }}</td>
                                </tr>
                                <tr>
                                    <th>Thời gian học:</th>
                                    <td>{{ $course->day_per_week }} buổi/ tuần</td>
                                </tr>
                                <tr>
                                    <th>Số học viên:</th>
                                    <td>{{ $course->min_student }} - {{ $course->max_student }} học viên</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row section-spacing content-detail">
                    <div class="col-xs-12 text-center">
                        <h2>Nội dung khóa học</h2>
                    </div>
                    <div class="participants">
                        <h3>Đối tượng tham gia khóa học {{ $course->title }}:</h2>
                        <div class="content">
                            {!! $course->participants !!}
                        </div>
                    </div>
                    <div class="detail">
                        <h3>Nội dung khóa học:</h2>
                        <div class="content">
                            {!! $course->detail !!}
                        </div>
                    </div>
                    <div class="results">
                        <h3>Kết quả đạt được:</h2>
                        <div class="content">
                            {!! $course->results !!}
                        </div>
                    </div>
                </div>
            </article>
            <div class="cta-btn text-center">
                <a target="_blank" class="btn subscribe-button" href="https://www.m.me/academy.fullstackcorp" class="btn subscribe-button">ĐĂNG KÝ NGAY</a>
                <p>
                    Giảm tối đa
                    <span class="discount">5.000.000 VNĐ</span>
                    khi đăng ký trước {{ date("t/m") }}
                </p>
            </div>
        </div>
        <div class="col-xs-12 cold-sm-12 col-md-3 col-lg-3">
            <aside class="sidebar sidebar-right">
                <div class="ads">
                    <div class="items">
                    <img src="{{ asset('assets/img/fullstack-ads.png') }}" alt="Banner sự kiện"
                            width="294" height="960">
                    </div>
                </div>
                <div class="section-spacing post-area">
                    <div class="title">
                        Latest Blogs
                    </div>
                    <div class="items">
                        {{-- <div class="row blog-item">
                            <div class="col-xs-5">
                                <img typeof="foaf:Image"
                                    src="">
                            </div>
                            <div class="col-xs-7">
                                <div>
                                    <a href="#">
                                        NƠI ẤY - MÃI MÃI LÀ NHÀ TÔI
                                    </a>
                                </div>
                                <div class="date">
                                    Mar 06,2020
                                </div>
                            </div>
                        </div>
                        <div class="row blog-item">
                            <div class="col-xs-5">
                                <img typeof="foaf:Image"
                                    src="#">
                            </div>
                            <div class="col-xs-7">
                                <div>
                                    <a href="#">
                                        NƠI ẤY - MÃI MÃI LÀ NHÀ TÔI
                                    </a>
                                </div>
                                <div class="date">
                                    Mar 06,2020
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </aside>
        </div>
    </div>
</div>
@include('landing.component.course')
@endsection
