<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Website Font style -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/three-dots.min.css">
        <link rel="stylesheet" href="assets/css/register_page.css">
        <link href="assets/css/main.css" rel="stylesheet">
        <title>Đăng ký học miễn phí</title>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

        <style>
        
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row main">
                <div class="panel-heading">

                   <div class="panel-title text-center">
                        <h1 class="title"><a href="{{ route('home') }}"style="color:black;">Fullstack Academy</a></h1>
                        <h3 class="title">Đăng ký học</h3>
                        <hr />
                    </div>
                </div> 
                <div class="main-login main-center" id="register">
                    
                    {{-- form --}}
                    <form id="register-form" method="post" action="">
                            <div class="form_group">
                                <label id="label">Họ và tên</label>
                                <input class="form-control" type="text" name="register_name" id="register-name">
                                <label class="bor"></label>
                            </div>
                            <div class="form_group">
                                <label id="label">Địa chỉ email</label>
                                <input class="form-control" type="email" name="register_email" id="register-email">
                                <label class="bor"></label>
                            </div>
                            <div class="form_group">
                                <label id="label">Số điện thoại</label>
                                <input class="form-control" type="number" name="register_phone" id="register-phone">
                                <label class="bor"></label>
                            </div>

                            <div class="form_group">
                                <label id="label">Ngày sinh</label>
                                <input class="form-control" type="date" name="register_birthday" id="register-birthday">
                                <label class="bor"></label>
                            </div>

                            <div class="form_group">
                                <label id="label">Địa chỉ</label>
                                <input class="form-control" type="text" name="register_address" id="register-address">
                                <label class="bor"></label>
                            </div>

                            <div class="form_group">
                                <label id="label">Nghề nghiệp</label>
                                <input class="form-control" type="text" name="register_job" id="register-job">
                                <label class="bor"></label>
                            </div>

                            <div class="form_group">
                                <label id="label">Link facebook</label>
                                <input class="form-control" type="text" name="link_fb" id="link-fb">
                                <label class="bor"></label>
                            </div>
                            <div class="frm_group">
                                <label id="label">Khóa học bạn chọn</label>
                                <select class="form-control form-select selcls" name="register_course" id="register-course">
                                    <option value="">---Khóa học---</option>
                                       @foreach($courses_free as $course)
                                            <option value="{{ isset($course) ? $course->id : '' }}">{{ isset($course) ? $course->title : '' }}</option>
                                        @endforeach
                                </select>
                                <label class="bor"></label>
                            </div>
                            <div class="form_group">
                                <label>Ghi chú</label>
                                <textarea class="form-control" rows="5" name="register_note" id="register-note" ></textarea>
                            </div>
                            <hr>
                            <div class="frm_group">
                                <button type="submit" id="btn-register" class="submit_register">
                                    Đăng ký! 
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                            </div>
                        </form>
                </div>
                <div class="panel-heading">
                   <div class="panel-title text-center">
                        <hr>
                        <h3 class="title">Địa chỉ học: số 22, ngõ 190 Nguyễn Trãi, Thanh Xuân, Hà Nội</h3>
                        <p>Thanks you !</p>
                        <hr />
                    </div>
                </div> 
            </div>
            
        </div>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="{{ asset('/assets/js/jquery.validate.min.js') }}"></script>
        <script src="assets/js/ajax.js"></script>

    </body>
</html>