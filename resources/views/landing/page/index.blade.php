@extends('landing.layout.master')

@section('pageTitle')
    FullStack Academy
@endsection

@section('content')

  <h1 style="visibility: hidden;height: 0px;padding: 0px;margin: 0px;">
    FullStack Academy - Học lập trình với giảng viên đến từ doanh nghiệp
  </h1>
  <!--hero section-->
  @include('landing.component.header')
  <!--hero section end-->
  @include('landing.component.featured-on')
  @include('landing.component.benefit')
  @include('landing.component.feature')
  @include('landing.component.course')
  @include('landing.component.reviews')
  @include('landing.component.cta')
  @include('landing.component.team')
  @include('landing.component.twitter')
  @include('landing.component.partners')
  @include('landing.component.contact')
  @include('landing.component.modal')
@endsection
