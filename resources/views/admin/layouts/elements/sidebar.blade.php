<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @php
                    $avatar = asset('assets/admin/dist/img/user2-160x160.jpg');
                    $admin = \Illuminate\Support\Facades\Auth::guard('admin')->user();
                    if (!empty($admin->avatar)) {
                        $avatar = $admin->avatar;
                    }
                @endphp
                <img src="{{ $avatar }}" class="img-circle" alt="{{ $admin->name }}"
                     style="width: 45px; height: 45px; object-fit: cover">
            </div>
            <div class="pull-left info">
                <p>{{ $admin->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">CHỨC NĂNG CHÍNH</li>
            @php
                $admin = \Illuminate\Support\Facades\Auth::guard('admin')->user();
                $action = app('request')->route()->getAction();
                $controller = class_basename($action['controller']);
                list($controller, $action) = explode('@', $controller);
                $controller = str_replace('Controller', '', $controller);
                $dashboardRoute = ['Dashboard'];

            @endphp

            <li class="treeview {{ in_array($controller, $dashboardRoute) ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Bảng tổng quan</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if($admin->can(config('permission.list.dashboard.all')))
                        <li class="{{ $controller == 'Dashboard' && $action == 'all' ? 'active' : '' }}">
                            <a href="{{ route('admin.dashboard.all') }}">
                                <i class="fa fa-dashboard"></i> <span>Bảng tổng quan tất cả</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>

            <?php
            $blogRouter = [
                'Blog', 'BlogCategory', 'BlogTag', 'BlogSearch'
            ];
            $blogRole = [
                config('permission.list.blog.list'),
                config('permission.list.blog_category.list'),
                config('permission.list.blog_tag.list'),
                config('permission.list.blog_search.list'),
            ];
            ?>
            @if(\App\Libs\CommonLib::checkRole($admin, $blogRole))
                <li class="treeview {{ in_array($controller, $blogRouter) ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-bookmark"></i>
                        <span>Blog</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($admin->can(config('permission.list.blog.list')))
                            <li class="{{ $controller == 'Blog' ? 'active' : '' }}">
                                <a href="{{ route('admin.blog.index') }}">
                                    <i class="fa fa-book"></i> Blog
                                </a>
                            </li>
                        @endif
                        @if($admin->can(config('permission.list.blog_category.list')))
                            <li class="{{ $controller == 'BlogCategory' ? 'active' : '' }}">
                                <a href="{{ route('admin.blog_category.index') }}">
                                    <i class="fa fa-list"></i> Blog Category
                                </a>
                            </li>
                        @endif
                        @if($admin->can(config('permission.list.blog_tag.list')))
                            <li class="{{ $controller == 'BlogTag' ? 'active' : '' }}">
                                <a href="{{ route('admin.blog_tag.index') }}">
                                    <i class="fa fa-tags"></i> Blog Tag
                                </a>
                            </li>
                        @endif
                        @if($admin->can(config('permission.list.blog_search.list')))
                            <li class="{{ $controller == 'BlogSearch' ? 'active' : '' }}">
                                <a href="{{ route('admin.blog_search.index') }}">
                                    <i class="fa fa-search"></i> Blog Search
                                </a>
                            </li>
                        @endif

                    </ul>
                </li>
            @endif

            @php
                $settingRole = [
                    config('permission.list.setting.list'),
                    config('permission.list.administrator.list'),
                    config('permission.list.role.list')
                ];
                $settingRoute = ['Setting', 'Administrator', 'Role'];
            @endphp
            @if(\App\Libs\CommonLib::checkRole($admin, $settingRole))
                <li class="treeview {{ in_array($controller, $settingRoute) ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span>{{ trans('admin.system_setting') }}</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @if($admin->can(config('permission.list.setting.list')))
                            <li class="{{ $controller == 'Setting' ? 'active' : '' }}">
                                <a href="{{ route('admin.setting.index') }}">
                                    <i class="fa fa-gear"></i> {{ trans('admin.common_setting') }}
                                </a>
                            </li>
                        @endif
                        @if($admin->can(config('permission.list.administrator.list')))
                            <li class="{{ $controller == 'Administrator' ? 'active' : '' }}">
                                <a href="{{ route('admin.administrator.index') }}">
                                    <i class="fa fa-user"></i> Quản lý tài khoản quản trị
                                </a>
                            </li>
                        @endif
                        @if($admin->can(config('permission.list.role.list')))
                            <li class="{{ $controller == 'Role' ? 'active' : '' }}">
                                <a href="{{ route('admin.role.index') }}">
                                    <i class="fa fa-user-secret"></i> Phân quyền
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>