@extends('admin.layouts.layout')
@php
    $pageTitle = !empty($pageTitle) ? $pageTitle : 'Import Blog Search';
@endphp
@section('title', $pageTitle)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $pageTitle }}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(!empty($record))
            {!! Form::model($record, ['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @else
            {!! Form::open(['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @endif
        <div class="row">
            <div class="col-sm-9">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ $pageTitle }}
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('list_keyword', 'List Keyword', ['class' => 'control-label']) !!}
                            <span class="text-red" style="font-weight: 600">*</span>
                            {!! Form::textarea('list_keyword', old('list_keyword'), ['class' => 'form-control', 'placeholder' => 'List keyword one each row' ]) !!}
                            <span class="help-block text-red validation_error hide validation_list_keyword"></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">

            </div>

            <div class="submit_fixed">
                <div class="content-wrapper text-center" style="background: #fff;">
                    <div class="col-sm-6">
                        <button class="btn btn-primary">
                            <i class="fa fa-save"></i> {{ $pageTitle }}
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <!-- /.content -->

@endsection