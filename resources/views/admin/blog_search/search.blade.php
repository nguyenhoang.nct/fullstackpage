{!! Form::open(['route' => 'admin.blog_search.batch_process', 'method' => 'POST', 'class' => 'frm_batch_process']) !!}

@include('admin.layouts.elements.record_statistic')

<div class="table-responsive">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="order_by {{ $request->order_by == 'blog_searches.id' ? 'active' : '' }}"
                data-order-by="blog_searches.id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">{{ trans('admin.id') }}
                {!! $request->order_by == 'blog_searches.id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_searches.keyword' ? 'active' : '' }}"
                data-order-by="blog_searches.keyword" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Keyword
                {!! $request->order_by == 'blog_searches.keyword' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_searches.updated_at' ? 'active' : '' }}"
                data-order-by="blog_searches.updated_at" data-order="{{ $request->order }}"
                data-form=".frm_form_search"> Updated
                {!! $request->order_by == 'blog_searches.updated_at' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($listData))
            @foreach($listData as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->keyword }}</td>
                    <td>{{ $value->updated_at }}</td>
                    <td>
                        <span class="help_tooltip" data-toggle="tooltip"
                              data-original-title="{{ trans('admin.edit_this_record') }}">
                            <a href="{{ route('admin.blog_search.edit', ['id' => $value->id]) }}"
                               class="btn btn-warning btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@include('admin.layouts.elements.paginate_and_action')

{{ Form::close() }}