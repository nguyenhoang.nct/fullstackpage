{!! Form::open(['route' => 'admin.blog_category.batch_process', 'method' => 'POST', 'class' => 'frm_batch_process']) !!}

@include('admin.layouts.elements.record_statistic')

<div class="table-responsive">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="order_by {{ $request->order_by == 'blog_categories.id' ? 'active' : '' }}"
                data-order-by="blog_categories.id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">{{ trans('admin.id') }}
                {!! $request->order_by == 'blog_categories.id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Ảnh đại diện</th>
            <th class="order_by {{ $request->order_by == 'blog_categories.name' ? 'active' : '' }}"
                data-order-by="blog_categories.name" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Tên danh mục
                {!! $request->order_by == 'blog_categories.name' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_categories.parent_id' ? 'active' : '' }}"
                data-order-by="blog_categories.parent_id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Danh mục cha
                {!! $request->order_by == 'blog_categories.parent_id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_categories.updated_at' ? 'active' : '' }}"
                data-order-by="blog_categories.updated_at" data-order="{{ $request->order }}"
                data-form=".frm_form_search"> Updated
                {!! $request->order_by == 'blog_categories.updated_at' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($listData))
            @foreach($listData as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <th>
                        @if(!empty($value->image))
                            <img src="{{ asset($value->image) }}" class="img-responsive"
                                 style="width: 100px; height: 60px; object-fit: cover"/>
                        @endif
                    </th>
                    <td>{{ $value->name }}</td>
                    <td>{{ !empty($value->parent) ? $value->parent->name : '' }}</td>
                    <td>{{ $value->updated_at }}</td>
                    <td>
                        <span class="help_tooltip" data-toggle="tooltip"
                              data-original-title="{{ trans('admin.edit_this_record') }}">
                            <a href="{{ route('admin.blog_category.edit', ['id' => $value->id]) }}"
                               class="btn btn-warning btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@include('admin.layouts.elements.paginate_and_action')

{{ Form::close() }}