@extends('admin.layouts.layout')
@php
    $pageTitle = !empty($pageTitle) ? $pageTitle : 'Thêm mới tin tức';
@endphp
@section('title', $pageTitle)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $pageTitle }}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(!empty($record))
            {!! Form::model($record, ['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @else
            {!! Form::open(['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @endif
        <div class="row">
            <div class="col-sm-9">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ $pageTitle }}
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('name', 'Tên danh mục', ['class' => 'control-label']) !!}
                            <span class="text-red" style="font-weight: 600">*</span>
                            {!! Form::text('name', old('name'), ['class' => 'form-control generate_alias',
                            'data-render' => 'render_link', 'placeholder' => 'Tên danh mục' ]) !!}
                            <span class="help-block text-red validation_error hide validation_name"></span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('alias', trans('admin.link'), ['class' => 'control-label']) !!} <span
                                    class="text-red" style="font-weight: 600">*</span>
                            {!! Form::text('alias', old('alias'), ['class' => 'form-control render_link', 'placeholder' => trans('admin.link')]) !!}
                            <span class="help-block text-red validation_error hide validation_alias"></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Meta Description</label>
                            <span class="text-red" style="font-weight: 600">*</span>
                            {!! Form::textarea('meta_description', old('meta_description'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Meta Description']) !!}
                            <span class="help-block text-red validation_error hide validation_meta_description"></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Meta Keyword</label>
                            <span class="text-red" style="font-weight: 600">*</span>
                            {!! Form::textarea('meta_keyword', old('meta_keyword'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Meta Keyword']) !!}
                            <span class="help-block text-red validation_error hide validation_meta_keyword"></span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Thuộc tính
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('parent_id', 'Danh mục cha', ['class' => 'control-label']) !!}
                            {!! Form::select('parent_id', ['' => 'Chọn danh mục cha'] + $listParent, old('parent_id'), ['class' => 'form-control select2']) !!}
                            <span class="help-block text-red validation_error hide validation_parent_id"></span>
                        </div>

                        <div class="form-group">
                            {!! Form::label('images', 'Ảnh đại diện', ['class' => 'control-label']) !!}
                            <span class="text-red" style="font-weight: 600">*</span>
                            <div class="avatar_container text-center form-group {{ empty($record) ? 'hide' : '' }}">
                                <img src="{{ !empty($record) ? asset($record->image) : '' }}" class="img_avatar img-responsive" />
                                {!! Form::hidden('image', old('image'), ['class' => 'input_avatar']) !!}
                            </div>
                            <div class="text-center">
                                <div class="btn btn-default btn_choose_avatar {{ !empty($record) ? 'hide' : '' }}">
                                    <i class="fa fa-image"></i> Click chọn ảnh đại diện
                                </div>
                                <div class="btn btn-danger btn_delete_avatar {{ empty($record) ? 'hide' : '' }}">
                                    <i class="fa fa-close"></i> Xóa ảnh đại diện
                                </div>
                            </div>
                            <span class="help-block text-red validation_error hide validation_avatar"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="submit_fixed">
                <div class="content-wrapper text-center" style="background: #fff;">
                    <div class="col-sm-6">
                        <button class="btn btn-primary">
                            <i class="fa fa-save"></i> {{ $pageTitle }}
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <!-- /.content -->

@endsection


@section('script')
    @include('admin.layouts.elements.ckeditor_script')

    <script type="text/javascript" language="JavaScript">
        $(document).ready(function () {
            $(document).on('click', '.delete_tag', function () {
                $(this).parent().parent().remove();
            });
        });
    </script>
@endsection