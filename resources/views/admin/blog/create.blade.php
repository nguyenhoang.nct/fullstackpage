@extends('admin.layouts.layout')
@php
    $pageTitle = !empty($pageTitle) ? $pageTitle : 'Thêm mới tin tức';
@endphp
@section('title', $pageTitle)
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>{{ $pageTitle }}</h1>
    </section>

    <!-- Main content -->
    <section class="content">
        @if(!empty($record))
            {!! Form::model($record, ['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @else
            {!! Form::open(['method' => 'POST', 'class' => 'frm_form_add']) !!}
        @endif
        <div class="row">
            <div class="col-sm-9">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            {{ $pageTitle }}
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('title', 'Tiêu đề tin', ['class' => 'control-label']) !!} <span
                                    class="text-red" style="font-weight: 600">*</span>
                            {!! Form::text('title', old('title'), ['class' => 'form-control generate_alias',
                            'data-render' => 'render_link', 'placeholder' => 'Tiêu đề tin' ]) !!}
                            <span class="help-block text-red validation_error hide validation_name"></span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('alias', trans('admin.link'), ['class' => 'control-label']) !!} <span
                                    class="text-red" style="font-weight: 600">*</span>
                            {!! Form::text('alias', old('alias'), ['class' => 'form-control render_link', 'placeholder' => trans('admin.link')]) !!}
                            <span class="help-block text-red validation_error hide validation_link"></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Meta description</label>
                            {!! Form::textarea('meta_description', old('meta_description'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Meta Description']) !!}
                            <span class="help-block text-red validation_error hide validation_meta_description"></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Meta Keyword</label>
                            {!! Form::textarea('meta_keyword', old('meta_keyword'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Meta Keyword']) !!}
                            <span class="help-block text-red validation_error hide validation_meta_keyword"></span>
                        </div>


                        <div class="form-group">
                            <label class="control-label">{{ trans('admin.description') }}</label>
                            {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Mô tả ngắn bài viết']) !!}
                            <span class="help-block text-red validation_error hide validation_description"></span>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Nội dung chi tiết</label>
                            <span class="help-block text-red validation_error hide validation_detail"></span>
                            {!! Form::textarea('detail', old('detail'), ['class' => 'form-control', 'id' => 'editor1']) !!}
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Thuộc tính bài viết
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            {!! Form::label('category_id', 'Danh mục tin', ['class' => 'control-label']) !!}
                            {!! Form::select('category_id', ['' => 'Chọn danh mục'] + $listCategory, old('category_id'), ['class' => 'form-control select2']) !!}
                            <span class="help-block text-red validation_error hide validation_category_id"></span>
                        </div>

                        <div class="form-group">
                            {!! Form::label('images', 'Ảnh đại diện', ['class' => 'control-label']) !!}
                            <div class="avatar_container text-center form-group {{ empty($record) ? 'hide' : '' }}">
                                <img src="{{ !empty($record) ? asset($record->images) : '' }}" class="img_avatar img-responsive" />
                                {!! Form::hidden('images', old('images'), ['class' => 'input_avatar']) !!}
                            </div>
                            <div class="text-center">
                                <div class="btn btn-default btn_choose_avatar {{ !empty($record) ? 'hide' : '' }}">
                                    <i class="fa fa-image"></i> Click chọn ảnh đại diện
                                </div>
                                <div class="btn btn-danger btn_delete_avatar {{ empty($record) ? 'hide' : '' }}">
                                    <i class="fa fa-close"></i> Xóa ảnh đại diện
                                </div>
                            </div>
                            <span class="help-block text-red validation_error hide validation_avatar"></span>
                        </div>
                    </div>
                </div>

                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Tags
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-9">
                                    <input type="text" placeholder="Tag mới" class="form-control input_tag" />
                                </div>
                                <div class="col-sm-3 no-padding-left-lg">
                                    <button type="button" class="btn btn-primary btn_add_tag">Add</button>
                                </div>
                            </div>
                            <span class="help-block text-red validation_error hide validation_tags"></span>
                        </div>

                        <div class="list_tag">
                            @if(!empty($record) && !empty($record->tags))
                                @foreach($record->tags as $key => $value)
                                    <div class="tag_item">
                                        <input type="hidden" name="tags[]" value="{{ $value->name }}" />
                                        <div class="label label-primary tag_{{ $value->alias }}">
                                            {{ $value->name }}  
                                            <label class="label label-warning delete_tag" style="cursor: pointer;"><i class="fa fa-close"></i></label>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>

            </div>

            <div class="submit_fixed">
                <div class="content-wrapper text-center" style="background: #fff;">
                    <div class="col-sm-6">
                        <button class="btn btn-primary">
                            <i class="fa fa-save"></i> {{ $pageTitle }}
                        </button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    <!-- /.content -->

@endsection


@section('script')
    @include('admin.layouts.elements.ckeditor_script')

    <script type="text/javascript" language="JavaScript">
        $(document).ready(function () {
            $(document).on('click', '.delete_tag', function () {
                $(this).parent().parent().remove();
            });

            $('.btn_add_tag').click(function () {
                var newTag = $('.input_tag').val();

                if(newTag != ''){
                    var html = `
                        <div class="tag_item">
                            <input type="hidden" name="tags[]" value="`+newTag+`" />
                            <div class="label label-primary">
                                `+newTag+`
                                <label class="label label-warning delete_tag" style="cursor: pointer;"><i class="fa fa-close"></i></label>
                            </div>
                        </div>`;

                    $('.list_tag').prepend(html);

                    $('.input_tag').val('');
                }
            });

            $('.input_tag').keypress(function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $('.btn_add_tag').click();
                }
            });
        });
    </script>
@endsection