{!! Form::open(['route' => 'admin.blog.batch_process', 'method' => 'POST', 'class' => 'frm_batch_process']) !!}

@include('admin.layouts.elements.record_statistic')

<div class="table-responsive">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="order_by {{ $request->order_by == 'blogs.id' ? 'active' : '' }}"
                data-order-by="blogs.id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">{{ trans('admin.id') }}
                {!! $request->order_by == 'blogs.id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Ảnh đại diện</th>
            <th class="order_by {{ $request->order_by == 'blogs.title' ? 'active' : '' }}"
                data-order-by="blogs.title" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Tiêu đề tin
                {!! $request->order_by == 'blogs.title' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blogs.category_id' ? 'active' : '' }}"
                data-order-by="blogs.blog_category_id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Danh mục tin
                {!! $request->order_by == 'blogs.category_id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blogs.updated_at' ? 'active' : '' }}"
                data-order-by="blogs.updated_at" data-order="{{ $request->order }}"
                data-form=".frm_form_search"> Updated
                {!! $request->order_by == 'blogs.updated_at' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($listData))
            @foreach($listData as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <th>
                        @if(!empty($value->images))
                            <img src="{{ asset($value->images) }}" class="img-responsive"
                                 style="width: 100px; height: 60px; object-fit: cover"/>
                        @endif
                    </th>
                    <td>{{ $value->title }}</td>
                    <td>{{ !empty($value->category) ? $value->category->name : '' }}</td>
                    <td>{{ $value->updated_at }}</td>
                    <td>
                        <span class="help_tooltip" data-toggle="tooltip"
                              data-original-title="{{ trans('admin.view_this_record') }}">
                            <a href="{{ route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]) }}"
                               target="_blank"
                               class="btn btn-primary btn-sm">
                                <i class="fa fa-eye"></i>
                            </a>
                        </span>
                        <span class="help_tooltip" data-toggle="tooltip"
                              data-original-title="{{ trans('admin.edit_this_record') }}">
                            <a href="{{ route('admin.blog.edit', ['id' => $value->id]) }}"
                               class="btn btn-warning btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@include('admin.layouts.elements.paginate_and_action')

{{ Form::close() }}