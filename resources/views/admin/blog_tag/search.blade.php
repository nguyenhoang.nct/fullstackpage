{!! Form::open(['route' => 'admin.blog_tag.batch_process', 'method' => 'POST', 'class' => 'frm_batch_process']) !!}

@include('admin.layouts.elements.record_statistic')

<div class="table-responsive">

    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th class="order_by {{ $request->order_by == 'blog_tags.id' ? 'active' : '' }}"
                data-order-by="blog_tags.id" data-order="{{ $request->order }}"
                data-form=".frm_form_search">{{ trans('admin.id') }}
                {!! $request->order_by == 'blog_tags.id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_tags.name' ? 'active' : '' }}"
                data-order-by="blog_tags.name" data-order="{{ $request->order }}"
                data-form=".frm_form_search">Tên tag
                {!! $request->order_by == 'blog_tags.name' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_tags.blog_id' ? 'active' : '' }}"
                data-order-by="blog_tags.blog_id" data-order="{{ $request->order }}"
                data-form=".frm_form_search"> Blog ID
                {!! $request->order_by == 'blog_tags.blog_id' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th class="order_by {{ $request->order_by == 'blog_tags.updated_at' ? 'active' : '' }}"
                data-order-by="blog_tags.updated_at" data-order="{{ $request->order }}"
                data-form=".frm_form_search"> Updated
                {!! $request->order_by == 'blog_tags.updated_at' ? ($request->order == 'ASC' ? '<i class="fa fa-caret-up"></i>' : '<i class="fa fa-caret-down"></i>') : '<i class="fa fa-sort"></i>' !!}
            </th>
            <th>Hành động</th>
        </tr>
        </thead>
        <tbody>
        @if(!empty($listData))
            @foreach($listData as $key => $value)
                <tr>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->blog_id.' - '.($value->blog ? $value->blog->title : '') }}</td>
                    <td>{{ $value->updated_at }}</td>
                    <td>
                        <span class="help_tooltip" data-toggle="tooltip"
                              data-original-title="{{ trans('admin.edit_this_record') }}">
                            <a href="{{ route('admin.blog_tag.edit', ['id' => $value->id]) }}"
                               class="btn btn-warning btn-sm">
                                <i class="fa fa-edit"></i>
                            </a>
                        </span>
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

@include('admin.layouts.elements.paginate_and_action')

{{ Form::close() }}