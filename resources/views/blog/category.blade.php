@extends('web.layouts.layout_blog')
@section('title', html_entity_decode($category->name))
@section('meta_description', html_entity_decode($category->meta_description))
@section('meta_keyword', html_entity_decode($category->meta_keyword))
@section('content')
    <div class="home_main">
        <div class="row">
            <div class="col-sm-9">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">Trang chủ</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('web.blog.index') }}">Blog</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{!! $category->name !!}</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <div class="block_title">
                    <div class="pull-left">
                        <a href="{{ route('web.blog.category', ['alias' => $category->alias]) }}">
                            {!! $category->name !!}
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                @include('web.blog.elements.list_post')

            </div>
            <div class="col-sm-3">
                @include('web.blog.elements.blog_sidebar')
            </div>
        </div>

    </div>

@endsection