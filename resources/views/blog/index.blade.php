@extends('landing.layout.master')
@section('pageTitle', 'Blog tin tức công nghệ, lập trình và đào tạo lập trình mới nhất - FullStack Academy')
@section('content')
@include('landing.component.header-only')
    <div class="home_top">
        <div class="row">
            <div class="col-sm-8">
                @if(!empty($newestPost[0]))
                    <?php $href = route('web.blog.detail', ['id' => $newestPost[0]->id, 'alias' => $newestPost[0]->alias]); ?>
                    <div class="block_title">
                        <div class="pull-left">
                            <a href="#">Tin nổi bật</a>
                        </div>
                        <div class="pull-right">
                            <a class="view_more" href="#">Xem thêm &raquo;</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row block_container">
                        <div class="col-sm-6">
                            <div class="hot_news_box">
                                <div class="hot_news_image">
                                    <a href="{{ $href }}">
                                        <img src="{{ $newestPost[0]->images }}" class="img-responsive"
                                             alt="{!! $newestPost[0]->title !!}"/>
                                    </a>
                                </div>
                                <div class="hot_news_title">
                                    <a href="{{ $href }}">
                                        <h3>{!! $newestPost[0]->title !!}</h3>
                                    </a>
                                </div>
                                <div class="hot_news_description">
                                    {!! $newestPost[0]->description !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            @foreach($newestPost as $key => $value)
                                @if($key > 0)
                                    <?php $href = route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]); ?>
                                    <div class="news_box">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-4">
                                                <div class="news_box_image">
                                                    <a href="{{ $href }}">
                                                        <img src="{{ $value->images }}" class="img-responsive"
                                                             alt="{!! $value->title !!}"/>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-sm-8 col-xs-8">
                                                <div class="news_box_title">
                                                    <a href="{{ $href }}">
                                                        <h3>
                                                            {!! $value->title !!}
                                                        </h3>
                                                    </a>
                                                </div>
                                                <div class="news_box_description">
                                                    {!! \Illuminate\Support\Str::words($value->description, 18, '...') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-sm-4">

            </div>
        </div>
    </div>

    <div class="home_main">
        <div class="row">
            <div class="col-sm-9">
                @if(!empty($category))
                    @foreach($category as $cat)
                        @php $catPost = $postByCategory[$cat->id] ?? []; @endphp
                        @if(!empty($catPost[0]))
                            <?php $href = route('web.blog.detail', ['id' => $catPost[0]->id, 'alias' => $catPost[0]->alias]); ?>
                            <div class="block_title">
                                <div class="pull-left">
                                    <a href="{{ route('web.blog.category', ['alias' => $cat->alias]) }}">{{ $cat->name }}</a>
                                </div>
                                <div class="pull-right">
                                    <a class="view_more" href="{{ route('web.blog.category', ['alias' => $cat->alias]) }}">Xem thêm &raquo;</a>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="row block_container">
                                <div class="col-sm-6">
                                    <div class="hot_news_box">
                                        <div class="hot_news_image">
                                            <a href="{{ $href }}">
                                                <img src="{{ $catPost[0]->images }}" class="img-responsive"
                                                     alt="{!! $catPost[0]->title !!}"/>
                                            </a>
                                        </div>
                                        <div class="hot_news_title">
                                            <a href="{{ $href }}">
                                                {!! $catPost[0]->title !!}
                                            </a>
                                        </div>
                                        <div class="hot_news_description">
                                            {!! $catPost[0]->description !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    @foreach($catPost as $key => $value)
                                        @if($key > 0)
                                            <?php $href = route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]); ?>
                                            <div class="news_box">
                                                <div class="row">
                                                    <div class="col-sm-4 col-xs-4">
                                                        <div class="news_box_image">
                                                            <a href="{{ $href }}">
                                                                <img src="{{ $value->images }}" class="img-responsive"
                                                                     alt="{!! $value->title !!}"/>
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-8 col-xs-8">
                                                        <div class="news_box_title">
                                                            <a href="{{ $href }}">
                                                                <h3>
                                                                    {!! $value->title !!}
                                                                </h3>
                                                            </a>
                                                        </div>
                                                        <div class="news_box_description">
                                                            {!! \Illuminate\Support\Str::words($value->description, 18, '...') !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="col-sm-3">
                @include('blog.elements.blog_sidebar')
            </div>
        </div>

    </div>

@endsection
