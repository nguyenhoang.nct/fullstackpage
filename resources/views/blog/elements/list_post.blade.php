@if(!empty($listPost) && !$listPost->isEmpty())
    @foreach($listPost as $value)
        <?php $href = route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]); ?>
        <div class="category_post_box">
            <div class="row">
                <div class="col-sm-4 col-xs-4 category_post_image">
                    <a href="{{ $href }}">
                        <img src="{{ env('APP_URL').'/'.$value->images }}" class="img-responsive"
                             alt="{!! $value->title !!}"/>
                    </a>
                </div>
                <div class="col-sm-8 col-xs-8">
                    <div class="category_post_title">
                        <a href="{{ $href }}">
                            <h3>{!! $value->title !!}</h3>
                        </a>
                    </div>
                    <div class="category_post_description">
                        {!! $value->description !!}
                    </div>
                </div>
            </div>
        </div>
    @endforeach

    <div class="pagination">
        {!! \App\Libs\CommonLib::prettyPaginationUrl($listPost) !!}
    </div>
@else
    <div class="text-center">Nội dung đang được cập nhật</div>
@endif