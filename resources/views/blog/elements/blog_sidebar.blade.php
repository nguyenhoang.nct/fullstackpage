<div class="sidebar_box">
    <div class="sidebar_box_title">Chuyên mục</div>
    <div class="sidebar_box_container">
        <ul>
            <?php $blogCategory = \App\Libs\WebLib::getBlogCategory(); ?>
            @if(!empty($blogCategory))
                @foreach($blogCategory as $key => $value)
                    <li>
                        <a href="{{ route('web.blog.category', ['alias' => $value->alias]) }}">
                            <h2>
                                {{ $value->name }}
                            </h2>
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>