@extends('web.layouts.layout_blog')
@section('title', html_entity_decode($tag->name))
@section('meta_description', html_entity_decode($tag->name))
@section('meta_keyword', html_entity_decode($tag->name))
@section('content')
    <div class="home_main">
        <div class="row">
            <div class="col-sm-9">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/">Trang chủ</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{ route('web.blog.index') }}">Blog</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">{!! $tag->name !!}</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">
                <div class="block_title">
                    <div class="pull-left">
                        <a href="{{ route('web.blog.tag', ['alias' => $tag->alias]) }}">
                            {!! $tag->name !!}
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                @include('web.blog.elements.list_post')

                @if(!$searchKeyword->isEmpty())
                    <div class="list_search_keyword post_page_tag">
                        <i class="fa fa-search"></i> Tìm kiếm liên quan:
                        @foreach($searchKeyword as $key => $value)
                            <a href="{{ route('web.blog.search', ['alias' => $value->alias]) }}">{{ $value->keyword }}</a>,
                        @endforeach
                    </div>
                @endif

            </div>
            <div class="col-sm-3">
                @include('web.blog.elements.blog_sidebar')
            </div>
        </div>

    </div>

@endsection