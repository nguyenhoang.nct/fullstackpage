@extends('web.layouts.layout_blog')
@section('title', html_entity_decode($post->title))
@section('content')
    <div class="home_main">
        <div class="row">
            <div class="col-sm-9">
                <ul class="breadcrumb" itemscope itemtype="https://schema.org/BreadcrumbList">
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem">
                        <a itemprop="item" href="/">
                            <span itemprop="name">Trang chủ</span>
                        </a>
                        <meta itemprop="position" content="1"/>
                    </li>
                    <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem">
                        <a itemscope itemtype="https://schema.org/WebPage"
                           itemprop="item" itemid="blogs" href="{{ route('web.blog.index') }}">
                            <span itemprop="name">Blogs</span></a>
                        <meta itemprop="position" content="2"/>
                    </li>
                    <?php $breadcrumbPosition = 2; ?>
                    @if($post->category)
                        <?php
                        $breadcrumbPosition++;
                        $href = route('web.blog.category', ['alias' => $post->category->alias]);
                        ?>
                        <li class="breadcrumb-item" itemprop="itemListElement" itemscope
                            itemtype="https://schema.org/ListItem">
                            <a itemscope itemtype="https://schema.org/WebPage"
                               itemprop="item" itemid="{{ $href }}" href="{{ $href }}">
                                <span itemprop="name">{!! $post->category->name !!}</span></a>
                            <meta itemprop="position" content="{{ $breadcrumbPosition }}"/>
                        </li>
                    @endif

                    <?php $breadcrumbPosition++; ?>
                    <li class="breadcrumb-item active" aria-current="page" itemprop="itemListElement" itemscope
                        itemtype="https://schema.org/ListItem">
                        <span itemprop="name">{!! $post->title !!}</span>
                        <meta itemprop="position" content="{{ $breadcrumbPosition }}"/>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-9">

                <h2 class="post_page_title">
                    {!! $post->title !!}
                </h2>

                <div class="post_page_info">
                    <ul>
                        <li>
                            <i class="fa fa-calendar"></i> {{ date('H:i d/m/Y', strtotime($post->created_at)) }}
                        </li>
                        <li>
                            <i class="fa fa-star"></i>
                            Xếp hạng 4.{{ $post->id%10 }}/5 với {{ $post->id % 23 }} phiếu bầu
                        </li>
                    </ul>
                </div>

                <div class="clearfix"></div>

                <div class="post_page_content">
                    {!! $post->detail !!}
                </div>

                <div class="post_page_source text-right">
                    <a href="{{ route('web.blog.detail', ['id' => $post->id, 'alias' => $post->alias]) }}">
                        {!! $post->title !!}
                    </a><br/>
                    <i>
                        <a href="{{ route('web.home.index') }}">{{ \App\Libs\WebLib::getSetting(\App\Models\Setting::SITE_NAME) }}</a>
                        @if(!empty($post->source))
                            tổng hợp theo: {{ $post->source }}
                        @endif
                    </i>
                </div>

                @if(!empty($listTag))
                    <div class="post_page_tag">
                        <i class="fa fa-tags"></i> Tag:
                        @foreach($listTag as $key => $value)
                            <a href="{{ route('web.blog.tag', ['alias' => $value->alias]) }}">{{ $value->name }}</a>,
                        @endforeach
                    </div>
                @endif

                <div class="form-group">
                    <div class="form-group">&nbsp;</div>
                </div>

                <div class="block_title">
                    <div class="pull-left">
                        Bài viết liên quan
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row block_container">
                    @if(!empty($relatePost))
                        @foreach($relatePost as $key => $value)
                            @if($key > 0)
                                <?php $href = route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]); ?>
                                <div class="relate_post_box col-sm-4">
                                    <div class="row">
                                        <div class="relate_post_image col-sm-12 col-xs-5">
                                            <a href="{{ $href }}">
                                                <img src="{{ $value->images ?? '' }}" class="img-responsive"
                                                     alt="{!! $value->title !!}"/>
                                            </a>
                                        </div>
                                        <div class="relate_post_title col-sm-12 col-xs-7">
                                            <a href="{{ $href }}">
                                                <h3>{!! $value->title !!}</h3>
                                            </a>
                                            <p>
                                                {!! str_limit($value->description, 80) !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @if($key % 3 == 2)
                                    <div class="clearfix"></div>
                                @endif
                            @endif
                        @endforeach
                    @endif
                </div>

                <div class="form-group">
                    <div class="form-group">&nbsp;</div>
                </div>

                <div class="block_title">
                    <div class="pull-left">
                        Cùng chuyên mục
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row block_container">
                    @if(!empty($sameCategory))
                        @foreach($sameCategory as $key => $value)
                            <?php $href = route('web.blog.detail', ['id' => $value->id, 'alias' => $value->alias]); ?>
                            <div class="relate_post_box col-sm-4">
                                <div class="row">
                                    <div class="relate_post_image col-sm-12 col-xs-5">
                                        <a href="{{ $href }}">
                                            <img src="{{ $value->images ?? '' }}" class="img-responsive"
                                                 alt="{!! $value->title !!}"/>
                                        </a>
                                    </div>
                                    <div class="relate_post_title col-sm-12 col-xs-7">
                                        <a href="{{ $href }}">
                                            <h3>{!! $value->title !!}</h3>
                                        </a>
                                        <p>
                                            {!! str_limit($value->description, 80) !!}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @if($key % 3 == 2)
                                <div class="clearfix"></div>
                            @endif
                        @endforeach
                    @endif
                </div>


            </div>
            <div class="col-sm-3">
                @include('web.blog.elements.blog_sidebar')
            </div>
        </div>
    </div>


    <script type="application/ld+json">
    {
        "@context": "https://schema.org",
        "@type": "NewsArticle",
        "mainEntityOfPage": {
            "@type": "WebPage",
            "@id": "{{ route('blog.index') }}"
        },
        "headline": "{{ $post->title }}",
        "image": ["{{ $post->images }}"],
        "datePublished": "{{ $post->created_at }}",
        "dateModified": "{{ $post->created_at }}",
        "author": {
            "@type": "Person",
            "name": "Nguyễn Hoàng Long"
        },
        "publisher": {
            "@type": "Organization",
            "name": "FullStack Academy",
            "logo": {
                "@type": "ImageObject",
                "url": "http://truyencotich.fun/assets/frontend/images/logo.png"
            }
        },
        "description": "{{ $post->description }}",
        "articleSection" : "{{ $post->category->name ?? 'Không có chuyên mục' }}"
    }
    </script>


@endsection
